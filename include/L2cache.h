/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#ifndef L2CACHE_H
#define L2CACHE_H

#include <netinet/in.h>
#include <string.h>

#include <L1cache.h>

using namespace std;
/*
    * ENUMERATIONS
*/

/* Return Values */
enum miss_hit {
    MISS,
    HIT
};

/*
 * STRUCTS
*/


/* Cache replacement policy results. Uses miss_hit as result field */
struct result {
    enum miss_hit miss_hit;
    bool dirty_eviction;
    long  evicted_address;
};

/*
*  Functions
*/

// Parses hit or miss from operation_result to result structure.
/*
    Input: operation_result.
    Output: result.
*/
void parse_miss_hit_result( operation_result* op_result, result* result);

// Updates stat count values
void multilevel_stats_count( result* L1_result,
        result* L2_result,
        int* L2_dirty_evictions,
        int* L1_misses,
        int* L2_misses,
        int* L1_hits,
        int* L2_hits,
        double* L1_miss_rate,
        double* L2_miss_rate,
        double* overall_miss_rate,
        int* overall_misses,
        int* overall_hits);

// Gets proper parameter sizes for index, tag and offset for each cache.
/*
    * Inputs: associativity, blocksize_bytes, cachesize_kb
    * Outputs: tag_size, idx_size, offset_size
*/
int multilevel_field_size_get(  int cachesize_kb,
        int associativity,
        int blocksize_bytes,
        int *L1_tag_size,
        int *L2_tag_size,
        int *L1_idx_size,
        int *L2_idx_size,
        int *L1_offset_size,
        int *L2_offset_size);

// Replacemente for the multilevel optimization.
int multilevel_lru_replacement (
        int L1_idx,
        int L2_idx,
        int L1_tag,
        int L2_tag,
        int L1_associativity,
        int L2_associativity,
        int L2_tag_size,
        int L1_idx_size,
        int L2_idx_size,
        int L1_offset_size,
        int L2_offset_size,
        bool loadstore,
        entry* L1_cache_blocks,
        entry* L2_cache_blocks,
        result* L1_result,
        result* L2_result,
        operation_result* tmp_l2_result,
        bool debug);

#endif
