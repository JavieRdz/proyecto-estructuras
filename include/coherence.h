/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#ifndef COHERENCE_H
#define COHERENCE_H

#include <netinet/in.h>
#include <string.h>

#include "L2cache.h"

using namespace std;
/*
    * ENUMERATIONS
*/

/* MESI values */
enum coherence_values {
    INVALID,
    SHARED,
    EXCLUSIVE,
    MODIFIED
};

/*
*  Functions
*/

// Returns miss or hit from cache using idx and tag.

int check_miss_hit(
    int idx,
    int tag,
    int associativity,
    entry* cache,
    bool debug
);

// Returns cp_value from cache using idx and tag
int check_cp(
    int idx,
    int tag,
    int associativity,
    entry* cache,
    bool debug
);

// Updates stat count values
void coherence_stats_count(
    result* L1_result,
    result* L2_result,
    int* L1_misses,
    int* L2_misses,
    int* L1_hits,
    int* L2_hits,
    int* L1_invalidations,
    int* L2_invalidations,
    bool invalid,
    bool CPU_select,
    double* L1_miss_rate,
    double* L2_miss_rate,
    double* overall_miss_rate
);

// Replaces entry in memory system and updates cp_values for MSI

int MSI_replacement(
    int L1_idx,
    int L2_idx,
    int L1_tag,
    int L2_tag,
    int L1_associativity,
    int L2_associativity,
    int L1_tag_size,
    int L2_tag_size,
    int L1_idx_size,
    int L2_idx_size,
    int L1_offset_size,
    int L2_offset_size,
    bool loadstore,
    bool* invalidation,
    entry* L1_cache_blocks,
    entry* L2_cache_blocks,
    entry* L3_cache_blocks,
    result* L1_result,
    result* L2_result,
    operation_result* tmp_l2_result,
    bool debug
);

// Replaces entry in memory system and updates cp_values for MESI

int MESI_replacement(
    int L1_idx,
    int L2_idx,
    int L1_tag,
    int L2_tag,
    int L1_associativity,
    int L2_associativity,
    int L1_tag_size,
    int L2_tag_size,
    int L1_idx_size,
    int L2_idx_size,
    int L1_offset_size,
    int L2_offset_size,
    bool loadstore,
    bool* invalidation,
    entry* L1_cache_blocks,
    entry* L2_cache_blocks,
    entry* L3_cache_blocks,
    result* L1_result,
    result* L2_result,
    operation_result* tmp_l2_result,
    bool debug
);

#endif
