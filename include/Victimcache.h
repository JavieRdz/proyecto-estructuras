/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#ifndef VICTIMCACHE_H
#define VICTIMCACHE_H

#include <netinet/in.h>
#include <string.h>

// Uncomment next lines for gtest simulation.
//#include <L1cache.h>

using namespace std;
/*
* ENUMERATIONS
*/
enum miss_hit_victim {
    MISSV,
    HITV,
    NONE
};

/*
* STRUCTS
*/

/* Victim cache tag array fields */

struct victim_cache {
    bool valid ;
    bool dirty;
    int address;
    uint8_t rp_value ;
};

/* Victim cache optimization results */

struct victim_result {
    enum miss_hit_victim miss_hit;
    int  evicted_address;
    bool dirty_eviction;
};

/*
*  Functions
*/
/*
 * Search for an address in a cache set and
 * replaces blocks using LRU policy and victim cache optimization.
 */
int victim_opt(int idx,
                            int tag,
                            int associativity,
                            int victim_associativity,
                            bool loadstore,
                            entry* cache_blocks,
                            victim_cache* victim_blocks,
                            operation_result* result,
                            victim_result* victim_result,
                            bool debug);

/*
 * Search for an address in a victim cache set and
 * replaces blocks using LRU policy.
 */
int victim_lru_replacement_policy (int address,
                                        int evicted_address,
                                        int victim_associativity,
                                        victim_cache* victim_blocks,
                                        victim_result* victim_result,
                                        bool dirty,
                                        bool debug);



#endif
