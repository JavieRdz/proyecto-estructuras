/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: j-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <time.h>

#include <coherence.h>
#include <debug_utilities.h>


#define KB 1024
#define ADDRSIZE 32
using namespace std;

int check_miss_hit(
    int idx,
    int tag,
    int associativity,
    entry* cache,
    bool debug
){
    for (int i = 0; i < associativity; i++){
        if ((cache + (idx * associativity + i))-> tag == tag && (cache + (idx * associativity + i))-> valid){
            return HIT; // If finds the tag and its valid, returns a HIT
        }
    }
    return MISS;
}


int check_cp(
    int idx,
    int tag,
    int associativity,
    entry* cache,
    bool debug
){
    int coherence = 0;
    for (int i = 0; i < associativity; i++){
        if ((cache + (idx * associativity + i))-> tag == tag){
            coherence = (cache + (idx * associativity + i)) -> cp_value; // Returns the coherence value if its a hit. Invalid if not
        }
    }
    return coherence;
}

void coherence_stats_count(
    result* CPU1_result,
    result* CPU2_result,
    int* CPU1_misses,
    int* CPU2_misses,
    int* CPU1_hits,
    int* CPU2_hits,
    int* CPU1_invalidations,
    int* CPU2_invalidations,
    bool invalid,
    bool CPU_select,
    double* CPU1_miss_rate,
    double* CPU2_miss_rate,
    double* overall_miss_rate
)
{
    if (!CPU_select){ // CPU1
        if(CPU1_result->miss_hit==MISS){
            *CPU1_misses = *CPU1_misses + 1;
        }
        else {
            *CPU1_hits = *CPU1_hits + 1;
        }
        if (invalid) *CPU1_invalidations = *CPU1_invalidations + 1;
    } else { // CPU2
        if(CPU2_result->miss_hit==MISS){
            *CPU2_misses = *CPU2_misses + 1;
        }
        else {
            *CPU2_hits = *CPU2_hits + 1;
        }
        if (invalid) *CPU2_invalidations = *CPU2_invalidations + 1;
    }

    *overall_miss_rate = (double) (*CPU1_misses + *CPU2_misses) / (double) ((*CPU1_misses + *CPU2_misses) + (*CPU1_hits + *CPU2_hits));
    *CPU1_miss_rate = (double) *CPU1_misses / (*CPU1_misses + *CPU1_hits);
    *CPU2_miss_rate = (double) *CPU2_misses / (*CPU2_misses + *CPU2_hits);
}


int MSI_replacement(
        int L1_idx,
        int L2_idx,
        int L1_tag,
        int L2_tag,
        int L1_associativity,
        int L2_associativity,
        int L1_tag_size,
        int L2_tag_size,
        int L1_idx_size,
        int L2_idx_size,
        int L1_offset_size,
        int L2_offset_size,
        bool loadstore,
        bool* invalidation,
        entry* L1_cache_blocks,
        entry* L2_cache_blocks,
        entry* L3_cache_blocks,
        result* L1_result,
        result* L2_result,
        operation_result* tmp_l2_result,
        bool debug
){
    int status = OK;
    *invalidation = false;

    status = multilevel_lru_replacement ( L1_idx, L2_idx, L1_tag, L2_tag, L1_associativity, L2_associativity, L2_tag_size, L1_idx_size, L2_idx_size, L1_offset_size, L2_offset_size, loadstore, L1_cache_blocks, L2_cache_blocks, L1_result, L2_result, tmp_l2_result, debug);

    if (L2_result->miss_hit == MISS){
        if (L2_result->evicted_address != 0) { // if there's eviction in L2
            int L3_idx = 0, L3_tag = 0;
            address_tag_idx_get(L2_result->evicted_address, L1_tag_size, L1_idx_size, L1_offset_size, &L3_idx, &L3_tag);
            if (check_miss_hit(L3_idx, L3_tag, L1_associativity, L3_cache_blocks, debug) == HIT ){ // Hit in L3 case. Invalidation
                for (int i = 0; i < L1_associativity; i++) {
                    if ((L3_cache_blocks + (L3_idx * L1_associativity + i )) -> tag == L3_tag){ // Searches for hit entry and invalidates it
                        (L3_cache_blocks + (L3_idx * L1_associativity + i )) -> cp_value = INVALID;
                        (L3_cache_blocks + (L3_idx * L1_associativity + i )) -> valid = false;
                        *invalidation = true;
                    }
                }
            } // Ends hit in L3 with eviction case
        } // Ends eviction in L2 case
        for (int i = 0; i < L1_associativity; i++){ // Any miss in L2 indicates that entry is in either cache_blocks

            if (!loadstore){ // Read case
                if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                    (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = SHARED; // False sharing because other is invalid.
                }
            }else { // Write case

                if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){
                    (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = MODIFIED; // Write: MODIFIED, in L3 is already invalid
                }
                if ((L3_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Searches for hit entry and invalidates it
                    (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = INVALID;
                    (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> valid = false;
                    *invalidation = true;
                }
            } // Ends write case
        } // Ends for associativity
    } else { // Hit in L2
        if (debug) printf("HIT IN L2\n");
        if (check_miss_hit(L1_idx, L1_tag, L1_associativity, L3_cache_blocks, debug) == HIT ){ // Hit also in L3
            for (int i = 0; i < L1_associativity; i++){ // Any miss in L2 indicates that entry is in either cache_blocks.
                if (!loadstore){ // Read case
                    if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                        (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = SHARED;
                    }
                    if ((L3_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L3
                        (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = SHARED;
                    }
                } else { // Write case
                    if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                        (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = MODIFIED;// Write: MODIFIED
                    }
                    if ((L3_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L3
                        (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = INVALID;// Invalidation
                        (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> valid = false;
                        *invalidation = true;
                    }
                } // Ends write case
            } // Ends for associativity
        } // Emds hit in L3
        else { // Hit with L1 cache
            for (int i = 0; i < L1_associativity; i++){ // Any miss in L2 indicates that entry is in either cache_blocks.
                if (!loadstore){ // Read case
                    if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                        (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = SHARED;
                    }
                } else { // Write case
                    if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                        (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = MODIFIED;// Write: MODIFIED
                    }
                } // Ends write case
            } // Ends for associativity
        }
    } // Ends hit in L2
    return status;
}// End MSI_replacement

int MESI_replacement(
        int L1_idx,
        int L2_idx,
        int L1_tag,
        int L2_tag,
        int L1_associativity,
        int L2_associativity,
        int L1_tag_size,
        int L2_tag_size,
        int L1_idx_size,
        int L2_idx_size,
        int L1_offset_size,
        int L2_offset_size,
        bool loadstore,
        bool* invalidation,
        entry* L1_cache_blocks,
        entry* L2_cache_blocks,
        entry* L3_cache_blocks,
        result* L1_result,
        result* L2_result,
        operation_result* tmp_l2_result,
        bool debug
){
    int status = OK;
    *invalidation = false;

    status = multilevel_lru_replacement ( L1_idx, L2_idx, L1_tag, L2_tag, L1_associativity, L2_associativity, L2_tag_size, L1_idx_size, L2_idx_size, L1_offset_size, L2_offset_size, loadstore, L1_cache_blocks, L2_cache_blocks, L1_result, L2_result, tmp_l2_result, debug);

    if (L2_result->miss_hit == MISS){
        if (debug) printf("MISS IN L2\n");
        if (L2_result->evicted_address != 0) { // if there's eviction in L2
            int L3_idx = 0, L3_tag = 0;
            address_tag_idx_get(L2_result->evicted_address, L1_tag_size, L1_idx_size, L1_offset_size, &L3_idx, &L3_tag);
            if (check_miss_hit(L3_idx, L3_tag, L1_associativity, L3_cache_blocks, debug) == HIT ){ // Hit in L3 case. Invalidation
                *invalidation = true; // Gets idx and tag for hit in L3
                for (int i = 0; i < L1_associativity; i++) {
                    if ((L3_cache_blocks + (L3_idx * L1_associativity + i )) -> tag == L3_tag){ // Searches for hit entry and invalidates it
                        (L3_cache_blocks + (L3_idx * L1_associativity + i )) -> cp_value = INVALID;
                        (L3_cache_blocks + (L3_idx * L1_associativity + i )) -> valid = false;
                    }
                    if ((L1_cache_blocks + (L3_idx * L1_associativity + i )) -> tag == L3_tag){ // Searches for hit entry, if its in SHARED, sets it to EXCLUSIVE
                        if ((L1_cache_blocks + (L3_idx * L1_associativity + i )) -> cp_value == SHARED) {
                            (L1_cache_blocks + (L3_idx * L1_associativity + i )) -> cp_value = EXCLUSIVE;
                        }
                    }
                }
            } // Ends hit in L3 with eviction case
        } // Ends eviction in L2 case
        for (int i = 0; i < L1_associativity; i++){ // Any miss in L2 indicates that entry is in either cache_blocks.
            if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1

                if (!loadstore){ // Read case
                    (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = EXCLUSIVE; // Other is invalid.
                } else { // Write case
                    (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = MODIFIED; // Write: MODIFIED, in L3 is already invalid
                    if ((L3_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Searches for hit entry and invalidates it
                        (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = INVALID;
                        (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> valid = false;
                        *invalidation = true;
                    }
                } // Ends write case
            } // Ends hit case in L1
        } // Ends for associativity
    } else { // Hit in L2
        if (debug) printf("HIT IN L2\n");
        if (check_miss_hit(L1_idx, L1_tag, L1_associativity, L3_cache_blocks, debug) == HIT ){ // Hit also in L3
            for (int i = 0; i < L1_associativity; i++){ // Any miss in L2 indicates that entry is in either cache_blocks.
                if (!loadstore){ // Read case
                    if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                        (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = SHARED;
                    }
                    if ((L3_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L3
                        (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = SHARED;
                    }
                } else { // Write case
                    if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                        (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = MODIFIED;// Write: MODIFIED
                    }
                    if ((L3_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L3
                        (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = INVALID;// Invalidation
                        (L3_cache_blocks + (L1_idx * L1_associativity + i )) -> valid = false;
                        *invalidation = true;
                    }
                } // Ends write case
            } // Ends for associativity
        } // Emds hit in L3
        else { // Hit with L1 cache
            for (int i = 0; i < L1_associativity; i++){ // Any miss in L2 indicates that entry is in either cache_blocks.
                if (!loadstore){ // Read case
                    if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                        (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = EXCLUSIVE;
                    }
                } else { // Write case
                    if ((L1_cache_blocks + (L1_idx * L1_associativity + i )) -> tag == L1_tag){ // Modifies cp_value for current entry in L1
                        (L1_cache_blocks + (L1_idx * L1_associativity + i )) -> cp_value = MODIFIED;// Write: MODIFIED
                    }
                } // Ends write case
            } // Ends for associativity
        }
    } // Ends hit in L2
    return status;
}// End MESI_replacement
