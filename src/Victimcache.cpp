/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: j-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <time.h>

// Uncomment next 2 lines for gtest simulations.
//#include <Victimcache.h>
//#include <debug_utilities.h>

// Comment next 2 lines for gtest simulations.
#include "./../include/Victimcache.h"
#include "./../include/debug_utilities.h"

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int victim_opt(int idx,
                            int tag,
                            int associativity,
                            int victim_associativity,
                            bool loadstore,
                            entry* cache_blocks,
                            victim_cache* victim_blocks,
                            operation_result* result,
                            victim_result* victim_result,
                            bool debug)
{

    bool rp_case = true;
    for(int i = 0; i < associativity; i++){
        if (!((cache_blocks + (idx * associativity + i)) -> valid)){ // empty miss case
            if (debug) printf("\n L1 empty case\n");
            (cache_blocks + (idx * associativity + i)) -> valid = true;
            (cache_blocks + (idx * associativity + i)) -> tag = tag;
            (cache_blocks + (idx * associativity + i)) -> rp_value = 0;
            for (int k = 0; k < associativity; k++) {
              if((cache_blocks + (idx * associativity + k)) -> tag!=tag){
                (cache_blocks + (idx * associativity + k)) -> rp_value++;
              }
            }
            if (loadstore){ // Store case
                (cache_blocks + (idx * associativity + i)) -> dirty = true;
                result -> miss_hit = MISS_STORE;
            } else { // Load case
                (cache_blocks + (idx * associativity + i)) -> dirty = false;
                result -> miss_hit = MISS_LOAD;
            }
            // Empty case: eviction doesn't happen.
            result -> evicted_address = 0;
            result -> dirty_eviction = false;
            i = associativity; // ends for
            rp_case = false; // Replacement not needed
            victim_result -> miss_hit = NONE;
            victim_result -> evicted_address = 0;
        } // End empty case
        else { // Valid case
            if ( (cache_blocks + (idx * associativity + i)) -> tag == tag) { // Hit case
                if (debug) printf("\nHit case\n");
                (cache_blocks + (idx * associativity + i)) -> tag = tag;
                for (int k = 0; k < associativity; k++){
                    if ( (cache_blocks + (idx * associativity + k))-> rp_value < (cache_blocks + (idx * associativity + i))-> rp_value){
                        (cache_blocks + (idx * associativity + k))-> rp_value = (cache_blocks + (idx * associativity + k))-> rp_value + 1;
                    }
                } // End for rp value
                (cache_blocks + (idx * associativity + i))-> rp_value = 0;

                if (loadstore){ // Store case
                    (cache_blocks + (idx * associativity + i)) -> dirty = true;
                    result -> miss_hit = HIT_STORE;
                } else { // Load case
                    result -> miss_hit = HIT_LOAD;
                }
                // Hit case: eviction doesn't happen.
                result -> evicted_address = 0;
                result -> dirty_eviction = false;
                i = associativity; // Exits for
                rp_case = false; // Replacement not needed
                victim_result -> miss_hit = NONE;
                victim_result -> evicted_address = 0;
            } //End hit case
        } // End valid case
    } //End for

    if (rp_case){ // Replace case
        if (debug) printf("\nL1 replace case\n");
        for (int i = 0; i < associativity; i++) {
            if ( (cache_blocks + (idx * associativity + i))-> rp_value == (associativity - 1) ){ //Replace case

                result -> evicted_address  = combine((cache_blocks+(idx*associativity+i)) -> tag,idx);
                // Sets dirty eviction to old dirty bit.
                result -> dirty_eviction = (cache_blocks + (idx * associativity + i)) -> dirty;

                victim_lru_replacement_policy (combine(tag,idx),// Address that will be verified in victim.
                                                        result->evicted_address, // LRU address from L1.
                                                        victim_associativity,
                                                        victim_blocks, // Victim Cache.
                                                        victim_result,
                                                        result->dirty_eviction, // Dirty bit of evicted address.
                                                        debug);

                (cache_blocks + (idx * associativity + i)) -> tag = tag; // Tag from main memory or victim cache.
                (cache_blocks + (idx * associativity + i)) -> rp_value = 0; //Lowest rp: newest data in line
                if (loadstore){ // Store case
                    (cache_blocks + (idx * associativity + i)) -> dirty = true;
                    result -> miss_hit = MISS_STORE;
                } else { // Load case
                    (cache_blocks + (idx * associativity + i)) -> dirty = false;
                    result -> miss_hit = MISS_LOAD;
                }
                if(victim_result->miss_hit==HITV && !loadstore){
                    // if hit case in victim and data were dirty then dirty bit in L1 need to be updated.
                    (cache_blocks + (idx * associativity + i)) -> dirty = victim_result->dirty_eviction;
                }
            } else { // Updates rp_value case
                (cache_blocks + (idx * associativity + i)) -> rp_value = (cache_blocks + (idx * associativity + i)) -> rp_value + 1;
            }
        } // End for
    } // End rp_case
    return OK;
}



int victim_lru_replacement_policy (int address,
                                        int evicted_address,
                                        int victim_associativity,
                                        victim_cache* victim_blocks,
                                        victim_result* victim_result,
                                        bool dirty,
                                        bool debug)
{
    if(debug){
        printf("Address es %d\n",address );
        printf("Evicted address es %d\n",evicted_address);
        printf("victim_associativity es %d\n",victim_associativity);
        for(int i = 0; i<victim_associativity;i++){
            printf("El contenido inicial de victim[%d] es %d y el rp es %d\n",i,(victim_blocks+i) -> address,(victim_blocks+i) -> rp_value );
        }
    }
    bool rp_case = true;
    bool lru_case=true;

    for(int i = 0; i < victim_associativity; i++){
        if ( (victim_blocks + i) -> address == address && (victim_blocks + i) -> valid==true ) { // Hit case
            if(debug) printf("\nHit case\n");
            if (debug) printf("\n Hit tag replacement case\n");
            (victim_blocks + i) -> address = evicted_address; // Position at Victim gets evicted address from L1.
            for (int k = 0; k < victim_associativity; k++){
                if ( (victim_blocks + k)-> rp_value < (victim_blocks + i)-> rp_value){ // Positions with lower rp value increase.
                    (victim_blocks + k)-> rp_value = (victim_blocks + k)-> rp_value + 1;
                }
            } // End for rp value
            (victim_blocks + i)-> rp_value = 0; //Lowest rp: newest data in line
            victim_result -> dirty_eviction =(victim_blocks + i)-> dirty ;
            (victim_blocks + i)->dirty=dirty;// Dirty bit from L1.
            victim_result -> miss_hit = HITV;
            victim_result -> evicted_address = 0;

            i = victim_associativity; // Exits for
            rp_case = false; // Replacement not needed
            if(debug){
                for(int i = 0; i<victim_associativity;i++){
                    printf("El contenido luego de hit es victim[%d] es %d y el valid es %d y el rp es %d\n",i,(victim_blocks+i) -> address,(victim_blocks+i) -> valid,(victim_blocks+i) -> rp_value );
                }
            }

        } //End hit case

    } //End for

    if (rp_case){ // Replace case
        if (debug) printf("\nReplace case\n");

        for (int i = 0; i < victim_associativity; i++) {
            if((victim_blocks+i)->valid==false){// Empty case.
                if (debug) printf("\nVictim Empty case\n");
                (victim_blocks+i)->address=evicted_address;
                (victim_blocks+i)->dirty=dirty;
                (victim_blocks+i)->valid=true;
                (victim_blocks+i)->rp_value=0;
                for (int k = 0; k < victim_associativity; k++) { //Other positions increase their rp value.
                  if((victim_blocks+k)->address!=evicted_address){
                    (victim_blocks+k)->rp_value=(victim_blocks+k)->rp_value+1;
                  }
                }
                i=victim_associativity; // Exit for.
                lru_case = false; // Replacement not needed
                victim_result -> evicted_address  = 0;
                victim_result -> miss_hit = MISSV;
                victim_result -> dirty_eviction = false;
                if(debug){
                    for(int i = 0; i<victim_associativity;i++){
                        printf("El contenido de victim[%d] es %d y el valid es %d y el rp es %d\n",i,(victim_blocks+i) -> address,(victim_blocks+i) -> valid,(victim_blocks+i) -> rp_value );
                    }
                }
            } // End empty case.
        }// End for

        if(lru_case){
            if (debug) printf("\nLRU case\n");
            for (int i = 0; i < victim_associativity; i++) {
                if ( (victim_blocks + i)-> rp_value == (victim_associativity - 1) ){ //Replace case

                    victim_result -> evicted_address  = (victim_blocks+i) -> address;
                    victim_result -> miss_hit = MISSV;
                    victim_result -> dirty_eviction = (victim_blocks + i)-> dirty;
                    if(debug)    printf("Bloque reemplazado es victim[%d] con el valor %d\n",i,(victim_blocks + i) -> address);
                    (victim_blocks + i) -> address = evicted_address;
                    (victim_blocks + i) -> valid = true;
                    (victim_blocks + i) -> rp_value = 0; //Lowest rp: newest data in line
                    (victim_blocks + i) -> dirty = dirty;

                } else { // Updates rp_value case
                    (victim_blocks + i) -> rp_value = (victim_blocks + i) -> rp_value + 1;
                }
            } // End for
        } // End lru_case
        }
    return OK;
}
