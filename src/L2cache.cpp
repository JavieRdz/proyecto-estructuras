/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: j-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <time.h>

#include <L2cache.h>
#include <debug_utilities.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

void parse_miss_hit_result( operation_result* op_result, result* result){
    if (op_result->miss_hit == MISS_STORE || op_result->miss_hit == MISS_LOAD) result->miss_hit = MISS;
    else result->miss_hit = HIT;
    result->dirty_eviction = op_result->dirty_eviction;
    result->evicted_address = op_result->evicted_address;
}

void multilevel_stats_count(   result* L1_result,
                    result* L2_result,
                    int* L2_dirty_evictions,
                    int* L1_misses,
                    int* L2_misses,
                    int* L1_hits,
                    int* L2_hits,
                    double* L1_miss_rate,
                    double* L2_miss_rate,
                    double* overall_miss_rate,
                    int* overall_misses,
                    int* overall_hits
                )
{
    if(L1_result->miss_hit==HIT || L2_result->miss_hit==HIT){
        *overall_hits = *overall_hits + 1;
    }
    else {
        *overall_misses = *overall_misses + 1;
    }
    if(L1_result->miss_hit==MISS){
        *L1_misses = *L1_misses + 1;
    }
    else {
        *L1_hits = *L1_hits + 1;
    }
    if(L2_result->miss_hit==MISS){
        *L2_misses = *L2_misses + 1;
    }
    else {
        *L2_hits = *L2_hits + 1;
    }
    if(L2_result -> dirty_eviction && L2_result -> evicted_address){
        *L2_dirty_evictions = *L2_dirty_evictions + 1;
    }
    *overall_miss_rate = (double) *overall_misses / (*overall_misses + *overall_hits);
    *L1_miss_rate = (double) *L1_misses / (*L1_misses + *L1_hits);
    *L2_miss_rate = (double) *L2_misses / (*L2_misses + *L2_hits);
}

int multilevel_field_size_get(  int cachesize_kb,
                                int associativity,
                                int blocksize_bytes,
                                int *L1_tag_size,
                                int *L2_tag_size,
                                int *L1_idx_size,
                                int *L2_idx_size,
                                int *L1_offset_size,
                                int *L2_offset_size){
    int status;
    status = field_size_get(cachesize_kb, associativity, blocksize_bytes, L1_tag_size, L1_idx_size, L1_offset_size); // Get sizes for L1
    status = field_size_get(cachesize_kb*4, associativity*2, blocksize_bytes, L2_tag_size, L2_idx_size, L2_offset_size); // Get size for L2
    return status;
}

int multilevel_lru_replacement (
        int L1_idx,
        int L2_idx,
        int L1_tag,
        int L2_tag,
        int L1_associativity,
        int L2_associativity,
        int L2_tag_size,
        int L1_idx_size,
        int L2_idx_size,
        int L1_offset_size,
        int L2_offset_size,
        bool loadstore,
        entry* L1_cache_blocks,
        entry* L2_cache_blocks,
        result* L1_result,
        result* L2_result,
        operation_result* tmp_l2_result,
        bool debug
){
    int *L2_idx_ignore=(int *)malloc(sizeof (int));
    //Check in L1
    bool rp_case = true;
    if (debug) printf("\n------------------\nAddress: %ld", combine_address(L1_idx, L1_tag, L1_idx_size, L1_offset_size));
    if (debug) {
        printf("\nL1 entry info: idx: %d tag: %d\n",
            L1_idx,
            L1_tag);
    }
    for(int i = 0; i < L1_associativity; i++){
        if (!((L1_cache_blocks + (L1_idx * L1_associativity + i))->valid)) { // empty case
            if (debug) printf("L1 empty case\n");
            (L1_cache_blocks + (L1_idx * L1_associativity + i))->valid = true;
            (L1_cache_blocks + (L1_idx * L1_associativity + i))->tag = L1_tag;
            (L1_cache_blocks + (L1_idx * L1_associativity + i))->rp_value = 0;
            for (int k = 0; k < L1_associativity; k++) {
                if((L1_cache_blocks + (L1_idx * L1_associativity + k)) -> tag!=L1_tag){
                    (L1_cache_blocks + (L1_idx * L1_associativity + k)) -> rp_value++;
                }
                //if (debug) printf("Entry: %d RP Value: %d\n", k, (L1_cache_blocks + (L1_idx * L1_associativity + k))-> rp_value);
            }
            i = L1_associativity; // ends for
            rp_case = false;
            L1_result->miss_hit = MISS;

            lru_replacement_policy (L2_idx, L2_tag, L2_idx_size, L2_offset_size, L2_associativity, loadstore, L2_cache_blocks, tmp_l2_result, debug);
            parse_miss_hit_result(tmp_l2_result, L2_result); // MUST BE MISS

        } //ends empty case
        else { // valid case
            if ((L1_cache_blocks + (L1_idx * L1_associativity + i))->tag == L1_tag){ // Hit case
                if (debug) printf("\nL1 hit case\n");
                (L1_cache_blocks + (L1_idx * L1_associativity + i)) -> tag = L1_tag;
                for (int k = 0; k < L1_associativity; k++){ // rp_value update
                    if ( (L1_cache_blocks + (L1_idx * L1_associativity + k))-> rp_value < (L1_cache_blocks + (L1_idx * L1_associativity + i))-> rp_value){
                        (L1_cache_blocks + (L1_idx * L1_associativity + k))-> rp_value = (L1_cache_blocks + (L1_idx * L1_associativity + k))-> rp_value + 1;
                    }
                } // End for rp value
                (L1_cache_blocks + (L1_idx * L1_associativity + i))-> rp_value = 0;
                i = L1_associativity; // Exits for
                rp_case = false;
                L1_result->miss_hit = HIT; // it doesnt matter in L1

                lru_replacement_policy (L2_idx, L2_tag, L2_idx_size, L2_offset_size, L2_associativity, loadstore, L2_cache_blocks, tmp_l2_result, debug); // Updates L2 rp_values
                parse_miss_hit_result(tmp_l2_result, L2_result);
            } // End hit case
        } // Ends valid case
    } // End for associativity

    if (rp_case){ // Replace case
        int evicted_tag;
        if (debug) printf("\nL1 replace case\n");
        for (int i = 0; i < L1_associativity; i++) {
            //if (debug) printf("Entry: %d RP value: %d\n", i, (L1_cache_blocks + (L1_idx * L1_associativity + i))-> rp_value );
            if ( (L1_cache_blocks + (L1_idx * L1_associativity + i))-> rp_value == (L1_associativity - 1) ){ //Replace case
                //if (debug) printf("\nReplacement successful\n");
                evicted_tag = (L1_cache_blocks + (L1_idx * L1_associativity + i)) -> tag;
                L1_result -> evicted_address  = combine_address(L1_idx, evicted_tag, L1_idx_size, L1_offset_size);
                (L1_cache_blocks + (L1_idx * L1_associativity + i)) -> tag = L1_tag;
                (L1_cache_blocks + (L1_idx * L1_associativity + i)) -> rp_value = 0; // MRU
            } else { // Updates rp_value case
                (L1_cache_blocks + (L1_idx * L1_associativity + i)) -> rp_value = (L1_cache_blocks + (L1_idx * L1_associativity + i)) -> rp_value + 1;
            }
        } // End for

        if (debug) printf("Evicted address: %ld | Evicted idx: %d | Evicted tag: %d | Idx_size %d | Offset size %d \n", combine_address(L1_idx, evicted_tag, L1_idx_size, L1_offset_size), L1_idx, evicted_tag, L1_idx_size, L1_offset_size);

        L1_result->miss_hit = MISS;
        address_tag_idx_get(combine_address(L1_idx, evicted_tag, L1_idx_size, L1_offset_size), L2_tag_size, L2_idx_size, L2_offset_size, L2_idx_ignore, &L2_tag); // Sets L2 idx and tag from evicted from L1

        lru_replacement_policy (L2_idx, L2_tag, L2_idx_size, L2_offset_size, L2_associativity, loadstore, L2_cache_blocks, tmp_l2_result, debug); // Replaces in L2 with L1 eviction

        address_tag_idx_get(combine_address(L1_idx, L1_tag, L1_idx_size, L1_offset_size), L2_tag_size, L2_idx_size, L2_offset_size, L2_idx_ignore, &L2_tag); // Sets L2 idx and tag from Writethrough

        lru_replacement_policy (L2_idx, L2_tag, L2_idx_size, L2_offset_size, L2_associativity, loadstore, L2_cache_blocks, tmp_l2_result, debug); // Writethrough

        parse_miss_hit_result(tmp_l2_result, L2_result);
        free(L2_idx_ignore);
    }// End replace case
    return OK;
}// End multilevel_lru_replacement
