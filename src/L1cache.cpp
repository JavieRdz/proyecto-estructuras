/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: j-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <time.h>
#include <pthread.h>

#include "L1cache.h"
#include <debug_utilities.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int combine(int a, int b) {
  int times = 10;
  while (times <= b)
     times *= 10;
  return a*times + b;
}

long combine_address(int idx, int tag, int idx_size, int offset_size){
    long address = tag;
    address = address << idx_size;
    address = address + idx;
    address = address << offset_size;
    return address;
}

void flags_management(int argc,
    char* argv [],
    int* cache_size,
    int* associativity,
    int* block_size,
    string* replacement_policy )
{
    string parameter;
    for(int i = 1; i<argc;i++){
        parameter = argv[i];
        if(parameter=="-t"){ // Cache size case
            *cache_size=atoi(argv[i+1]);
            i++;
        }
        if(parameter=="-a"){ // Asoc case
            *associativity=atoi(argv[i+1]);
            i++;
        }
        if(parameter=="-l"){ // Block size case
            *block_size=atoi(argv[i+1]);
            i++;
        }
        if(parameter=="-cp"){ // RP case
            *replacement_policy = argv[i+1];
            i++;
        }
    }
}

int replacement_policy_select( int* rp )
{
    srand(time(0)); // Sets seed to "time"
    if (*rp == LRU){
        printf("\n\tReplacement policy selected: LRU\n");
        return OK;
    } else if (*rp == SRRIP){
        printf("\n\tReplacement policy selected: SRRIP\n");
        return OK;
    } else if (*rp == NRU){
        printf("\n\tReplacement policy selected: NRU\n");
        return OK;
    } else if (*rp == RANDOM){
        *rp = rand() % 2; // Asigns a rand int from 0 to 1.
        printf("\n\tRandom replacement policy selected: ");
        if (*rp == SRRIP) {
            printf("SRRIP\n");
        } else if (*rp == LRU) {
            printf("LRU\n");
        } else if (*rp == NRU) {
            printf("NRU\n");
        }
        return OK;
    } else {
        printf("Invalid replacement policy value\n");
        return ERROR;
    }
}

void replacement_policy_replace(int replacement_policy,
                                int idx,
                                int tag,
                                int idx_size,
                                int offset_size,
                                int associativity,
                                bool loadstore,
                                entry* cache_blocks,
                                operation_result* operation_result,
                                bool debug)
{
    if(replacement_policy){ // SRRIP case
      srrip_replacement_policy(idx, tag, associativity, loadstore, cache_blocks, operation_result, debug);
    }
    else{ // LRU case
      lru_replacement_policy(idx, tag, idx_size, offset_size, associativity, loadstore, cache_blocks, operation_result, debug);
    }
}


void stats_count( int miss_penalty,
                int hit_time,
                operation_result* result,
                int* dirty_evictions,
                int* total_misses,
                int* total_hits,
                int* miss_store,
                int* miss_load,
                int* hit_store,
                int* hit_load,
                double* overall_miss_rate,
                double* read_miss_rate,
                int* cpu,
                int* amat,
                int* ic )
{
    if(result->miss_hit==MISS_LOAD || result->miss_hit==MISS_STORE ){
        *total_misses = *total_misses + 1;
        if(result->miss_hit==MISS_LOAD){
            *cpu = *cpu + *ic+ miss_penalty;
            *miss_load = *miss_load + 1;
        }
        else {
            *miss_store = *miss_store + 1;
            *cpu = *cpu + *ic;
        }

    }
    else {
        if(result->miss_hit==HIT_LOAD){
        *hit_load = *hit_load + 1;
        }
        else{
            *hit_store = *hit_store + 1;
        }
        *cpu = *cpu + *ic;
        *total_hits = *total_hits + 1;
    }
    if(result -> dirty_eviction && result -> evicted_address){
        *dirty_evictions = *dirty_evictions + 1;
    }
    *overall_miss_rate = (double) *total_misses/( *total_hits  +  *total_misses);
    *read_miss_rate  = (double) *miss_load/( *hit_load  + *miss_load);
    *amat  =  hit_time + (*overall_miss_rate)*miss_penalty;
}


int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size)
{
    if(cachesize_kb <= 0
        || associativity <= 0
        || blocksize_bytes <= 0
        || ((associativity & (associativity - 1)) != 0)// No power of 2 case
        || ((cachesize_kb & (cachesize_kb - 1)) != 0)// No power of 2 case
        || ((blocksize_bytes & (blocksize_bytes - 1)) != 0) // No power of 2 case
        || (cachesize_kb*1024 < blocksize_bytes || cachesize_kb*1024 < associativity)
    ){
        return PARAM;
    } else {

        int field_size = 0, cache_size = 0, asoc_size = 0, block_size = 0, idx = 0, tag = 0;
        const double log2 = log(2.0);
        cache_size =  10.0+log(cachesize_kb) / log2; // 10 for kb; logA(B) / logA(C) = LogC(B)
        asoc_size =  log(associativity) / log2; // logA(B) / logA(C) = LogC(B)
        block_size = log(blocksize_bytes) / log2; // logA(B) / logA(C) = LogC(B)
        field_size = ADDRSIZE; //same as address size

        *offset_size = block_size;
        idx = (cache_size-block_size) - asoc_size; // log2(cachesize_kb / associativity)
        *idx_size = idx;
        tag = field_size - block_size - idx; //rest of address.
        *tag_size = tag;
        return OK;
    }
}

void address_tag_idx_get(long address,
                        int tag_size,
                        int idx_size,
                        int offset_size,
                        int *idx,
                        int *tag)
{
  int tmp;
  int mask;

  tmp = address >>  offset_size;  //  Se elimina el offset.
  mask  = pow(2,idx_size)-1;//Se realiza una máscara cantidad de unos igual al idx_size.
  *idx  = tmp & mask; //  Se obtiene el index.

  //Tag
  *tag  = tmp >>  idx_size; // Se elimina el index de tmp y queda el tag.

}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{

  int j = 0;
  int srrip=1;

  if(associativity<=0) return PARAM;
  while(j<associativity){

    if((cache_blocks+(idx*associativity+j))->valid==false){
      //Bloque de caché inicialmente vacío.
      (cache_blocks+(idx*associativity+j)) ->  valid = true;
      (cache_blocks+(idx*associativity+j)) ->  tag = tag;
      //dirty true si es store y false si es load.
      loadstore?(cache_blocks+(idx*associativity+j)) ->  dirty = true:(cache_blocks+(idx*associativity+j))  ->  dirty = false;
      //Bloque nuevo ingresa con rp_value 0 para NRU y rp_value para SRRIP.
      associativity<=2?(cache_blocks+(idx*associativity+j))  ->  rp_value  = 0:(cache_blocks+(idx*associativity+j))  ->  rp_value  = 2;
      loadstore?result  ->  miss_hit  = MISS_STORE:result  ->  miss_hit  =  MISS_LOAD;
      result  ->  dirty_eviction  = false;
      result  ->  evicted_address = 0;
      j=associativity;//Se deja de buscar en la línea.
      srrip=0;// No se salta al caso de reemplazo.
    }
    else if((cache_blocks+(idx*associativity+j))->valid==true && (cache_blocks+(idx*associativity+j))->tag==tag){//Caso de hit.

      if(loadstore){// Si es un store, se cambia el dirty a true.

        (cache_blocks+(idx*associativity+j)) ->  dirty = true;

      }

      (cache_blocks+(idx*associativity+j)) ->  rp_value  = 0;//Bloque ingresa con rp_value 0 para NRU y SRRIP.
      loadstore?result  ->  miss_hit  = HIT_STORE:result  ->  miss_hit  = HIT_LOAD;
      result  ->  dirty_eviction  = false;
      result  ->  evicted_address = 0;
      j=associativity;//Se deja de buscar en la línea.
      srrip=0;// No se salta al caso de reemplazo.
    }
    j++;//Si no coincide el tag ni el bloque está vacío se sigue buscando en la línea.
  }

  if(srrip){//Caso de reemplazo.
    j = 0;
    if(associativity<=2){//
      while((cache_blocks+(idx*associativity+j)) ->  rp_value!=1){// Para NRU se busca el bloque que tenga rp_value=1;
        if(j<associativity) j++;
        else{
          for(j=0;j<associativity;j++){// Si no se encuentra el 1, entonces todos los rp_value pasan a ser 1.
            (cache_blocks+(idx*associativity+j)) ->  rp_value = 1;
          }
          j = 0;
        }
      }
      loadstore?result  ->  miss_hit = MISS_STORE:result  ->  miss_hit = MISS_LOAD;
      result  ->  dirty_eviction  = (cache_blocks+(idx*associativity+j)) -> dirty;//dirty_eviction es el dirty del bloque reemplazado.
      //evicted_address es la concatenación del tag seguido del index.
      result  ->  evicted_address  = combine((cache_blocks+(idx*associativity+j)) -> tag,idx);
      (cache_blocks+(idx*associativity+j)) ->  tag  = tag;
      (cache_blocks+(idx*associativity+j)) ->  rp_value = 0;
      loadstore?(cache_blocks+(idx*associativity+j)) ->  dirty  = true:(cache_blocks+(idx*associativity+j)) ->  dirty = false;
    }
    else{//Para SRRIP se busca el rp_value igual a 3.
      j =  0;
      while((cache_blocks+(idx*associativity+j)) ->  rp_value!=3){
        if(j<associativity) j++;
        else{
          for(j=0;j<associativity;j++){// Si no se encuentra el 3 se le suma 1 a todos los rp_value.
            ((cache_blocks+(idx*associativity+j)) ->  rp_value)++;
          }
          j = 0;
        }
      }
      loadstore?result  ->  miss_hit = MISS_STORE:result  ->  miss_hit =  MISS_LOAD;
      result  ->  dirty_eviction  = (cache_blocks+(idx*associativity+j)) -> dirty;
      result  ->  evicted_address  = combine((cache_blocks+(idx*associativity+j)) -> tag,idx);
      (cache_blocks+(idx*associativity+j)) ->  tag  = tag;
      (cache_blocks+(idx*associativity+j)) ->  rp_value = 2;// El bloque ingresa con un rp_value de 2.
      loadstore?(cache_blocks+(idx*associativity+j)) ->  dirty  = true:(cache_blocks+(idx*associativity+j)) ->  dirty = false;
    }
  }
  if (debug) {
    printf("Result Info\n miss_hit: %d\n dirty_eviction: %d\n evicted_address: %ld\n",
          result  ->  miss_hit,
          result  ->  dirty_eviction,
          result  ->  evicted_address);
  }

  return OK;

}

int lru_replacement_policy (
    int idx,
    int tag,
    int idx_size,
    int offset_size,
    int associativity,
    bool loadstore,
    entry* cache_blocks,
    operation_result* result,
    bool debug
)
{
    bool rp_case = true;
    if (debug) {
        printf("L2 entry info: idx: %d tag: %d\n",
            idx,
            tag);
    }
    for(int i = 0; i < associativity; i++){
        if (!((cache_blocks + (idx * associativity + i)) -> valid)){ // empty miss case
            if (debug) printf("L2 empty case\n");
            (cache_blocks + (idx * associativity + i)) -> valid = true;
            (cache_blocks + (idx * associativity + i)) -> tag = tag;
            (cache_blocks + (idx * associativity + i)) -> rp_value = 0;
            for (int k = 0; k < associativity; k++) {
              if((cache_blocks + (idx * associativity + k)) -> tag!=tag){
                (cache_blocks + (idx * associativity + k)) -> rp_value++;
              }
            }
            if (loadstore){ // Store case
                (cache_blocks + (idx * associativity + i)) -> dirty = true;
                result -> miss_hit = MISS_STORE;
            } else { // Load case
                (cache_blocks + (idx * associativity + i)) -> dirty = false;
                result -> miss_hit = MISS_LOAD;
            }
            // Empty case: eviction doesn't happen.
            result -> evicted_address = 0;
            result -> dirty_eviction = false;
            i = associativity; // ends for
            rp_case = false; // Replacement not needed
        } // End empty case
        else { // Valid case
            if ( (cache_blocks + (idx * associativity + i)) -> tag == tag) { // Hit case
                if (debug) printf("L2 hit case\n");
                //if (debug) printf("\nHit case\n");
                (cache_blocks + (idx * associativity + i)) -> tag = tag;
                for (int k = 0; k < associativity; k++){
                    if ( (cache_blocks + (idx * associativity + k))-> rp_value < (cache_blocks + (idx * associativity + i))-> rp_value){
                        (cache_blocks + (idx * associativity + k))-> rp_value = (cache_blocks + (idx * associativity + k))-> rp_value + 1;
                    }
                } // End for rp value
                (cache_blocks + (idx * associativity + i))-> rp_value = 0;

                if (loadstore){ // Store case
                    (cache_blocks + (idx * associativity + i)) -> dirty = true;
                    result -> miss_hit = HIT_STORE;
                } else { // Load case
                    result -> miss_hit = HIT_LOAD;
                }
                // Hit case: eviction doesn't happen.
                result -> evicted_address = 0;
                result -> dirty_eviction = false;
                i = associativity; // Exits for
                rp_case = false; // Replacement not needed
            } //End hit case
        } // End valid case
    } //End for

    if (rp_case){ // Replace case
        if (debug) printf("\nL2 replace case\n");
        for (int i = 0; i < associativity; i++) {
            if ( (cache_blocks + (idx * associativity + i))-> rp_value == (associativity - 1) ){ //Replace case
                //if (debug) printf("\nReplacement successful\n");
                result -> evicted_address  = combine_address(idx, (cache_blocks+(idx*associativity+i)) -> tag, idx_size, offset_size);
                // Sets dirty eviction to old dirty bit.
                result -> dirty_eviction = (cache_blocks + (idx * associativity + i)) -> dirty;


                (cache_blocks + (idx * associativity + i)) -> tag = tag;
                (cache_blocks + (idx * associativity + i)) -> rp_value = 0; //Lowest rp: newest data in line
                if (loadstore){ // Store case
                    (cache_blocks + (idx * associativity + i)) -> dirty = true;
                    result -> miss_hit = MISS_STORE;
                } else { // Load case
                    (cache_blocks + (idx * associativity + i)) -> dirty = false;
                    result -> miss_hit = MISS_LOAD;
                }
            } else { // Updates rp_value case
                (cache_blocks + (idx * associativity + i)) -> rp_value = (cache_blocks + (idx * associativity + i)) -> rp_value + 1;
            }
        } // End for*/
    } // End rp_case
    return OK;
}
