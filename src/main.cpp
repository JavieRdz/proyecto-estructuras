#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <time.h>
#include <pthread.h>

#include <debug_utilities.h>
#include "coherence.h"

#include "coherence.cpp"
#include "L1cache.cpp"
#include "L2cache.cpp"

#define MISS_PENALTY 20
#define HIT_TIME 1
/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char* argv []) {


    int associativity, block_size;
    int* cache_size = (int *)malloc(sizeof (int));
    string coherence_protocol;

    //Main flags management
    flags_management(argc, argv, cache_size, &associativity, &block_size, &coherence_protocol);

    //if (coherence){ // Use this part if using coherence protocols.
        if (coherence_protocol == "MSI"){
            int final=0;
            int debug = false, CPU_select = false;
            bool invalidation = false;
            int loadstore, ic, CPU1_hits  = 0, CPU1_misses = 0, CPU2_hits  = 0, CPU2_misses = 0, CPU1_invalidations  = 0, CPU2_invalidations = 0;
            double CPU1_miss_rate = 0.0, CPU2_miss_rate = 0.0, overall_miss_rate = 0.0, time_st, time_end;
            long address;
            char tmp_ls;
            int counter = 0;

            int *CPU1_tag_size=(int *)malloc(sizeof (int));
            int *CPU1_idx_size=(int *)malloc(sizeof (int));
            int *CPU1_offset_size=(int *)malloc(sizeof (int));
            int *L2_tag_size=(int *)malloc(sizeof (int));
            int *L2_idx_size=(int *)malloc(sizeof (int));
            int *L2_offset_size=(int *)malloc(sizeof (int));

            int CPU1_idx, CPU1_tag, CPU1_rows, L2_idx, L2_tag, L2_rows;
            int CPU1_associativity = 0, L2_associativity = 0;
            int status;

            struct result CPU1_result={};
            struct result CPU2_result={};
            struct result L2_result={};
            struct operation_result L2_result_tmp={};

            CPU1_associativity = associativity;
            L2_associativity = 2*CPU1_associativity;

            time_st=clock(); //Start counting time

            // Sets parameters
            status = multilevel_field_size_get(*cache_size, CPU1_associativity, block_size, CPU1_tag_size, L2_tag_size, CPU1_idx_size, L2_idx_size, CPU1_offset_size, L2_offset_size);
            printf("L1 sizes: %d %d %d\nL2 sizes: %d %d %d\n\n", *CPU1_tag_size, *CPU1_idx_size, *CPU1_offset_size, *L2_tag_size, *L2_idx_size, *L2_offset_size );

            CPU1_rows=pow(2,*CPU1_idx_size);
            L2_rows=pow(2,*L2_idx_size);

            printf("\tStatus: %d\n", status);
            printf("\tRunning MSI Coherence Protocol\n");

            struct entry CPU1_cache[CPU1_rows][CPU1_associativity];
            struct entry CPU2_cache[CPU1_rows][CPU1_associativity];
            struct entry L2_cache[L2_rows][L2_associativity];

            // Fills both cache with invalid data.
            for (int i = 0; i < CPU1_rows; i++) {
                for (int j = 0; j < CPU1_associativity; j++) {
                    CPU1_cache[i][j].valid=false;
                    CPU2_cache[i][j].valid=false;

                    CPU1_cache[i][j].cp_value = INVALID;
                    CPU2_cache[i][j].cp_value = INVALID;
                }
            }
            for (int i = 0; i < L2_rows; i++) {
                for (int j = 0; j < L2_associativity; j++) {
                    L2_cache[i][j].valid=false;
                }
            }

            while(final>=0){
                final = scanf("%s",&tmp_ls);
                if(final>0){
                    scanf("%d\n",&loadstore);
                    scanf("%lX\n",&address);
                    scanf("%d\n",&ic);
                    counter++;
                    if (counter%4 == 0){
                        CPU_select = false;
                    } else {
                        CPU_select = true;
                    }
                    //printf("Address: %ld\n", address);

                    // Gets index and tag from address for each cache
                    address_tag_idx_get(address, *CPU1_tag_size, *CPU1_idx_size, *CPU1_offset_size, &CPU1_idx, &CPU1_tag);
                    address_tag_idx_get(address, *L2_tag_size, *L2_idx_size, *L2_offset_size, &L2_idx, &L2_tag);

                    // Proper MSI multilevel lru replacement
                    if (!CPU_select){

                        MSI_replacement(CPU1_idx, L2_idx, CPU1_tag, L2_tag, CPU1_associativity, L2_associativity, *CPU1_tag_size, *L2_tag_size, *CPU1_idx_size, *L2_idx_size, *CPU1_offset_size, *L2_offset_size, loadstore, &invalidation, CPU1_cache[0], L2_cache[0], CPU2_cache[0], &CPU1_result, &L2_result, &L2_result_tmp, debug);

                    } else {
                        MSI_replacement(CPU1_idx, L2_idx, CPU1_tag, L2_tag, CPU1_associativity, L2_associativity, *CPU1_tag_size, *L2_tag_size, *CPU1_idx_size, *L2_idx_size, *CPU1_offset_size, *L2_offset_size, loadstore, &invalidation, CPU2_cache[0], L2_cache[0], CPU1_cache[0], &CPU2_result, &L2_result, &L2_result_tmp, debug);

                    }

                    //Updates stat count
                    coherence_stats_count(&CPU1_result, &CPU2_result, &CPU1_misses, &CPU2_misses, &CPU1_hits, &CPU2_hits, &CPU1_invalidations, &CPU2_invalidations, invalidation, CPU_select, &CPU1_miss_rate, &CPU2_miss_rate, &overall_miss_rate);
                }
            }

            time_end=clock(); // Ends counting time

            printf("\n\tCache parameters\n\n");
            printf("\tL1 Cache size (KB): \t  %d\n",*cache_size);
            printf("\tL2 Cache size (KB): \t  %d\n",(*cache_size)*4);
            printf("\tL1 Cache Associativity:\t  %d\n",CPU1_associativity);
            printf("\tL2 Cache Associativity:\t  %d\n",L2_associativity);
            printf("\tCache Block Size (bytes): %d\n",block_size);
            printf("\tCoherence protocol: MSI\n\n");

            printf("\tSimulation results\n\n");
            printf("\tOverall miss rate:  %f\n", overall_miss_rate);
            printf("\tCPU1 L1 miss rate:    %f\n", CPU1_miss_rate);
            printf("\tCPU2 L1 miss rate:    %f\n", CPU2_miss_rate);
            printf("\tCoherence Invalidations CPU1:    %d\n", CPU1_invalidations);
            printf("\tCoherence Invalidations CPU2:    %d\n", CPU2_invalidations);

            printf("\tExecution time (s): %f\n\n",(((time_end - time_st))/CLOCKS_PER_SEC));

            free(CPU1_tag_size);
            free(CPU1_idx_size);
            free(CPU1_offset_size);
            free(L2_tag_size);
            free(L2_idx_size);
            free(L2_offset_size);
        } else if (coherence_protocol == "MESI") {
            int final=0;
            int debug = false, CPU_select = false;
            bool invalidation = false;
            int loadstore, ic, CPU1_hits  = 0, CPU1_misses = 0, CPU2_hits  = 0, CPU2_misses = 0, CPU1_invalidations  = 0, CPU2_invalidations = 0;
            double CPU1_miss_rate = 0.0, CPU2_miss_rate = 0.0, overall_miss_rate = 0.0, time_st, time_end;
            long address;
            int counter = 0;
            char tmp_ls;

            int *CPU1_tag_size=(int *)malloc(sizeof (int));
            int *CPU1_idx_size=(int *)malloc(sizeof (int));
            int *CPU1_offset_size=(int *)malloc(sizeof (int));
            int *L2_tag_size=(int *)malloc(sizeof (int));
            int *L2_idx_size=(int *)malloc(sizeof (int));
            int *L2_offset_size=(int *)malloc(sizeof (int));

            int CPU1_idx, CPU1_tag, CPU1_rows, L2_idx, L2_tag, L2_rows;
            int CPU1_associativity = 0, L2_associativity = 0;
            int status;

            struct result CPU1_result={};
            struct result CPU2_result={};
            struct result L2_result={};
            struct operation_result L2_result_tmp={};

            CPU1_associativity = associativity;
            L2_associativity = 2*CPU1_associativity;

            time_st=clock(); //Start counting time

            // Sets parameters
            status = multilevel_field_size_get(*cache_size, CPU1_associativity, block_size, CPU1_tag_size, L2_tag_size, CPU1_idx_size, L2_idx_size, CPU1_offset_size, L2_offset_size);
            printf("L1 sizes: %d %d %d\nL2 sizes: %d %d %d\n\n", *CPU1_tag_size, *CPU1_idx_size, *CPU1_offset_size, *L2_tag_size, *L2_idx_size, *L2_offset_size );

            CPU1_rows=pow(2,*CPU1_idx_size);
            L2_rows=pow(2,*L2_idx_size);

            printf("\tStatus: %d\n", status);
            printf("\tRunning MESI Coherence Protocol\n");

            struct entry CPU1_cache[CPU1_rows][CPU1_associativity];
            struct entry CPU2_cache[CPU1_rows][CPU1_associativity];
            struct entry L2_cache[L2_rows][L2_associativity];

            // Fills both cache with invalid data.
            for (int i = 0; i < CPU1_rows; i++) {
                for (int j = 0; j < CPU1_associativity; j++) {
                    CPU1_cache[i][j].valid=false;
                    CPU2_cache[i][j].valid=false;

                    CPU1_cache[i][j].cp_value = INVALID;
                    CPU2_cache[i][j].cp_value = INVALID;
                }
            }
            for (int i = 0; i < L2_rows; i++) {
                for (int j = 0; j < L2_associativity; j++) {
                    L2_cache[i][j].valid=false;
                }
            }

            while(final>=0){
                final = scanf("%s",&tmp_ls);
                if(final>0){
                    scanf("%d\n",&loadstore);
                    scanf("%lX\n",&address);
                    scanf("%d\n",&ic);
                    counter++;
                    if (counter%4 == 0){
                        CPU_select = false;
                    } else {
                        CPU_select = true;
                    }
                    //printf("Address: %ld\n", address);

                    // Gets index and tag from address for each cache
                    address_tag_idx_get(address, *CPU1_tag_size, *CPU1_idx_size, *CPU1_offset_size, &CPU1_idx, &CPU1_tag);
                    address_tag_idx_get(address, *L2_tag_size, *L2_idx_size, *L2_offset_size, &L2_idx, &L2_tag);

                    // Proper MSI multilevel lru replacement
                    if (!CPU_select){

                        MESI_replacement(CPU1_idx, L2_idx, CPU1_tag, L2_tag, CPU1_associativity, L2_associativity, *CPU1_tag_size, *L2_tag_size, *CPU1_idx_size, *L2_idx_size, *CPU1_offset_size, *L2_offset_size, loadstore, &invalidation, CPU1_cache[0], L2_cache[0], CPU2_cache[0], &CPU1_result, &L2_result, &L2_result_tmp, debug);

                    } else {
                        MESI_replacement(CPU1_idx, L2_idx, CPU1_tag, L2_tag, CPU1_associativity, L2_associativity, *CPU1_tag_size, *L2_tag_size, *CPU1_idx_size, *L2_idx_size, *CPU1_offset_size, *L2_offset_size, loadstore, &invalidation, CPU2_cache[0], L2_cache[0], CPU1_cache[0], &CPU2_result, &L2_result, &L2_result_tmp, debug);

                    }

                    //Updates stat count
                    coherence_stats_count(&CPU1_result, &CPU2_result, &CPU1_misses, &CPU2_misses, &CPU1_hits, &CPU2_hits, &CPU1_invalidations, &CPU2_invalidations, invalidation, CPU_select, &CPU1_miss_rate, &CPU2_miss_rate, &overall_miss_rate);
                }
            }

            time_end=clock(); // Ends counting time

            printf("\n\tCache parameters\n\n");
            printf("\tL1 Cache size (KB): \t  %d\n",*cache_size);
            printf("\tL2 Cache size (KB): \t  %d\n",(*cache_size)*4);
            printf("\tL1 Cache Associativity:\t  %d\n",CPU1_associativity);
            printf("\tL2 Cache Associativity:\t  %d\n",L2_associativity);
            printf("\tCache Block Size (bytes): %d\n",block_size);
            printf("\tCoherence protocol: MSI\n\n");

            printf("\tSimulation results\n\n");
            printf("\tOverall miss rate:  %f\n", overall_miss_rate);
            printf("\tCPU1 L1 miss rate:    %f\n", CPU1_miss_rate);
            printf("\tCPU2 L1 miss rate:    %f\n", CPU2_miss_rate);
            printf("\tCoherence Invalidations CPU1:    %d\n", CPU1_invalidations);
            printf("\tCoherence Invalidations CPU2:    %d\n", CPU2_invalidations);

            printf("\tExecution time (s): %f\n\n",(((time_end - time_st))/CLOCKS_PER_SEC));

            free(CPU1_tag_size);
            free(CPU1_idx_size);
            free(CPU1_offset_size);
            free(L2_tag_size);
            free(L2_idx_size);
            free(L2_offset_size);
        } else {
            printf("Error choosing coherence protocol\n" );
            return ERROR;
        }
/*
    } else {
        if(optimization==SIMPLE){

            int final=0;
            int loadstore, ic, total_hits  = 0, total_misses = 0, cpu  = 0, hit_load  = 0, hit_store = 0, miss_load = 0, miss_store = 0, amat  = 0, dirty_evictions = 0;
            double overall_miss_rate = 0.0, read_miss_rate = 0.0, time_st, time_end;
            long address;
            char tmp_ls;
            string parameter;
            int *tag_size=(int *)malloc(sizeof (int));
            int *idx_size=(int *)malloc(sizeof (int));
            int *offset_size=(int *)malloc(sizeof (int));
            int idx,tag,rows;
            int status;

            struct operation_result result={};

            printf("Running Simple Cache\n");

            time_st=clock(); //Start counting time
            status = field_size_get(*cache_size,associativity,block_size,tag_size,idx_size,offset_size);
            rows=pow(2,*idx_size);
            printf("\tStatus: %d\n", status);
            struct entry cache[rows][associativity];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < associativity; j++) {
                    cache[i][j].valid=false;
                }
            }

            while(final>=0){
                final = scanf("%s",&tmp_ls);
                if(final>0){
                    scanf("%d\n",&loadstore);
                    scanf("%lX\n",&address);
                    scanf("%d\n",&ic);

                    address_tag_idx_get(address,*tag_size,*idx_size,*offset_size,&idx,&tag);

                    replacement_policy_replace(0, idx, tag, associativity, loadstore, cache[0], &result, false);

                    stats_count(MISS_PENALTY, HIT_TIME, &result, &dirty_evictions, &total_misses, &total_hits, &miss_store, &miss_load, &hit_store, &hit_load, &overall_miss_rate, &read_miss_rate, &cpu, &amat, &ic );
                }
            }

            time_end=clock(); // Ends counting time

            printf("\n\tCache parameters\n\n");
            printf("\tCache size (KB): \t  %d\n",*cache_size);
            printf("\tCache Associativity: \t  %d\n",associativity);
            printf("\tCache Block Size (bytes): %d\n\n",block_size);

            printf("\tSimulation results\n\n");
            printf("\tCPU time (cycles):  %d\n",cpu );
            printf("\tAMAT (cycles):      %d\n", amat);
            printf("\tOverall miss rate:  %f\n", overall_miss_rate);
            printf("\tRead miss rate:     %f\n", read_miss_rate);
            printf("\tDirty evictions:    %d\n",dirty_evictions);
            printf("\tLoad misses: \t    %d\n",miss_load);
            printf("\tStore misses: \t    %d\n",miss_store);
            printf("\tTotal misses: \t    %d\n",total_misses);
            printf("\tLoad hits:\t    %d\n",hit_load);
            printf("\tStore hits:\t    %d\n",hit_store);
            printf("\tTotal hits: \t    %d\n",total_hits);
            printf("\tExecution time (s): %f\n\n",(((time_end - time_st))/CLOCKS_PER_SEC));

            free(tag_size);
            free(idx_size);
            free(offset_size);

        } // End simple cache
        if(optimization==MULTILEVEL) { // Start multilevel optimization
            int final=0;
            int debug = false;
            int loadstore, ic, L1_hits  = 0, L1_misses = 0, L2_hits  = 0, L2_misses = 0, overall_misses = 0, overall_hits = 0, dirty_evictions = 0;
            double L1_miss_rate = 0.0, L2_miss_rate = 0.0, overall_miss_rate = 0.0, time_st, time_end;
            long address;
            char tmp_ls;

            int *L1_tag_size=(int *)malloc(sizeof (int));
            int *L1_idx_size=(int *)malloc(sizeof (int));
            int *L1_offset_size=(int *)malloc(sizeof (int));
            int *L2_tag_size=(int *)malloc(sizeof (int));
            int *L2_idx_size=(int *)malloc(sizeof (int));
            int *L2_offset_size=(int *)malloc(sizeof (int));

            int L1_idx, L1_tag, L1_rows, L2_idx, L2_tag, L2_rows;
            int L1_associativity = 0, L2_associativity = 0;
            int status;

            struct result L1_result={};
            struct result L2_result={};
            struct operation_result L2_result_tmp={};

            L1_associativity = associativity;
            L2_associativity = 2*L1_associativity;

            time_st=clock(); //Start counting time

            // Sets parameters
            status = multilevel_field_size_get(*cache_size, L1_associativity, block_size, L1_tag_size, L2_tag_size, L1_idx_size, L2_idx_size, L1_offset_size, L2_offset_size);
            printf("L1 sizes: %d %d %d\nL2 sizes: %d %d %d\n", *L1_tag_size, *L1_idx_size, *L1_offset_size, *L2_tag_size, *L2_idx_size, *L2_offset_size );

            L1_rows=pow(2,*L1_idx_size);
            L2_rows=pow(2,*L2_idx_size);

            printf("\tStatus: %d\n", status);
            printf("\tRunning Multilevel Optimization\n");

            struct entry L1_cache[L1_rows][L1_associativity];
            struct entry L2_cache[L2_rows][L2_associativity];

            // Fills both cache with invalid data.
            for (int i = 0; i < L1_rows; i++) {
                for (int j = 0; j < L1_associativity; j++) {
                    L1_cache[i][j].valid=false;
                }
            }
            for (int i = 0; i < L2_rows; i++) {
                for (int j = 0; j < L2_associativity; j++) {
                    L2_cache[i][j].valid=false;
                }
            }

            while(final>=0){
                final = scanf("%s",&tmp_ls);
                if(final>0){
                    scanf("%d\n",&loadstore);
                    scanf("%lX\n",&address);
                    scanf("%d\n",&ic);
                    //printf("Address: %ld\n", address);

                    // Gets index and tag from address for each cache
                    address_tag_idx_get(address, *L1_tag_size, *L1_idx_size, *L1_offset_size, &L1_idx, &L1_tag);
                    address_tag_idx_get(address, *L2_tag_size, *L2_idx_size, *L2_offset_size, &L2_idx, &L2_tag);

                    // Proper multilevel replacement
                    multilevel_lru_replacement(L1_idx, L2_idx, L1_tag, L2_tag, L1_associativity, L2_associativity, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, L1_cache[0], L2_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

                    //Updates stat count
                    multilevel_stats_count(&L1_result, &L2_result, &dirty_evictions, &L1_misses, &L1_hits, &L2_misses, &L2_hits,  &L1_miss_rate, &L2_miss_rate, &overall_miss_rate, &overall_misses, &overall_hits );
                }
            }

            time_end=clock(); // Ends counting time

            printf("\n\tCache parameters\n\n");
            printf("\tL1 Cache size (KB): \t  %d\n",*cache_size);
            printf("\tL2 Cache size (KB): \t  %d\n",(*cache_size)*4);
            printf("\tL1 Cache Associativity:\t  %d\n",L1_associativity);
            printf("\tL2 Cache Associativity:\t  %d\n",L2_associativity);
            printf("\tCache Block Size (bytes): %d\n\n",block_size);

            printf("\tSimulation results\n\n");
            printf("\tOverall misses:  %d\n", overall_misses);
            printf("\tOverall hits:  %d\n", overall_hits);
            printf("\tOverall miss rate:  %f\n", overall_miss_rate);
            printf("\tL1 miss rate:    %f\n", L1_miss_rate);
            printf("\tL2 miss rate:    %f\n", L2_miss_rate);

            printf("\tMisses (L1):    %d\n", L1_misses);
            printf("\tHits (L1):    %d\n", L1_hits);
            printf("\tMisses (L2):    %d\n", L2_misses);
            printf("\tHits (L2):    %d\n", L2_hits);

            printf("\tDirty evictions:    %d\n",dirty_evictions);
            printf("\tExecution time (s): %f\n\n",(((time_end - time_st))/CLOCKS_PER_SEC));

            free(L1_tag_size);
            free(L1_idx_size);
            free(L1_offset_size);
            free(L2_tag_size);
            free(L2_idx_size);
            free(L2_offset_size);
        }
        if(optimization==VICTIM){
            int final=0;
            int debug = false;
            int loadstore, ic, total_hits  = 0, total_misses = 0, cpu  = 0, hit_load  = 0, hit_store = 0, miss_load = 0, miss_store = 0, amat  = 0, dirty_evictions = 0, victim_hit=0,victim_miss=0;
            double overall_miss_rate = 0.0, read_miss_rate = 0.0, time_st, time_end;
            long address;
            char tmp_ls;
            string parameter;
            int victim_associativity=16;
            int *tag_size=(int *)malloc(sizeof (int));
            int *idx_size=(int *)malloc(sizeof (int));
            int *offset_size=(int *)malloc(sizeof (int));
            int idx,tag,rows;
            int status;

            struct operation_result result={};
            struct victim_result victim_result={};
            time_st=clock(); //Start counting time


            status = field_size_get(*cache_size,associativity,block_size,tag_size,idx_size,offset_size);
            rows=pow(2,*idx_size);
            printf("Running Victim Cache Optimization\n");
            printf("\tStatus: %d\n", status);
            struct entry cache[rows][associativity];
            struct victim_cache victim_blocks[victim_associativity];

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < associativity; j++) {
                  cache[i][j].valid=false;
                }

              }

            for(int i=0;i<victim_associativity;i++){
                victim_blocks[i].valid=false;

            }

            while(final>=0){
                final = scanf("%s",&tmp_ls);
                if(final>0){

                scanf("%d\n",&loadstore);
                scanf("%lX\n",&address);
                scanf("%d\n",&ic);

                address_tag_idx_get(address,*tag_size,*idx_size,*offset_size,&idx,&tag);

                victim_opt(idx, tag, associativity,victim_associativity, loadstore, cache[0],victim_blocks,&result,&victim_result, debug);

                if(victim_result.miss_hit==HITV){
                    victim_hit++;
                }
                if(victim_result.miss_hit==MISSV){
                    victim_miss++;
                }
                if(result.miss_hit==MISS_LOAD || result.miss_hit==MISS_STORE ){
                    total_misses = total_misses + 1;
                if(result.miss_hit==MISS_LOAD){
                    cpu = cpu + ic+ MISS_PENALTY;
                    miss_load = miss_load + 1;
                }
                else {
                    miss_store = miss_store + 1;
                    cpu = cpu + ic;
                }

                }
                else {
                    if(result.miss_hit==HIT_LOAD){
                        hit_load = hit_load + 1;
                }
                else{
                    hit_store = hit_store + 1;
                }
                    cpu = cpu + ic;
                    total_hits = total_hits + 1;
                }
                if(victim_result.dirty_eviction && victim_result.evicted_address &&victim_result.miss_hit==MISSV){
                    dirty_evictions = dirty_evictions + 1;
                }
                    overall_miss_rate = (double) (total_misses+victim_miss)/( total_hits+victim_hit  +  total_misses + victim_miss);
                    read_miss_rate  = (double) miss_load/( hit_load  + miss_load);
                    amat  =  HIT_TIME + (overall_miss_rate)*MISS_PENALTY;
                }
            }

            time_end=clock(); // Ends counting time
            printf("\n\tCache parameters\n\n");
            printf("\tL1 Cache size (KB): \t  %d\n",*cache_size);
            printf("\tL1 Cache Associativity:\t  %d\n",associativity);
            printf("\tCache Block Size (bytes): %d\n\n",block_size);

            printf("\tSimulation results\n\n");
            printf("\tMiss rate (L1 + VC):  %f\n", overall_miss_rate);
            printf("\tMisses (L1 + VC):  %d\n", (total_misses+victim_miss));
            printf("\tHits (L1 + VC):  %d\n", (total_hits+victim_hit));
            printf("\tVictim cache hits:  %d\n", victim_hit);
            printf("\tVictim cache misses:  %d\n", victim_miss);
            printf("\tDirty evictions:    %d\n",dirty_evictions);
            printf("\tExecution time (s): %f\n\n",(((time_end - time_st))/CLOCKS_PER_SEC));
        }
    }*/
    return 0;
}
