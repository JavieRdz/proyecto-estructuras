/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>

#include "./../src/Victimcache.cpp"
#include "./../src/L2cache.cpp"
#include "./../src/coherence.cpp"
#define YEL   "\x1B[33m"

/* Globals */
int debug_on = 1;

/* Test Helpers */
#define DEBUG(x) if (debug_on) printf("%s\n",#x)

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
 /*
TEST(L1cache, hit_miss_srrip){
  int status;
  int i;
  int j;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry *
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  struct entry cache_line[idx][associativity];
  /* Check for a miss *
  DEBUG(Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line *
    for ( j =  0; j < associativity; j++) {
      cache_line[idx][j].valid = true;
      cache_line[idx][j].tag = rand()%4096;
      cache_line[idx][j].dirty = 0;
      cache_line[idx][j].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[idx][j].tag == tag) {
        cache_line[idx][j].tag = rand()%4096;
      }
    }
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line[0],
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same
   * tag now we will get a hit
   *
  DEBUG(Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx,
                                     tag,
                                     associativity,
                                     loadstore,
                                     cache_line[0],
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 *
TEST(L1cache, hit_miss_lru) {
    int status = 0;
    int idx = 0;
    int tag = 0;
    int associativity = 0;
    enum miss_hit_status expected_miss_hit = MISS_STORE;
    int loadstore = 1;
    bool debug = 0;
    struct operation_result result = {};

    /* Fill a random cache entry *
    idx = rand()%1024;
    tag = rand()%4096;
    associativity = 1 << (rand()%4);
    if (debug_on) {
        printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
            idx,
            tag,
            associativity);
    }

    struct entry cache_line[idx][associativity];
    /* Check for a miss *
    DEBUG(\nChecking miss operation);
    for (int k = 0 ; k < 2; k++){
        /* Fill cache line *
        for (int j =  0; j < associativity; j++) {
            cache_line[idx][j].valid = true;
            cache_line[idx][j].tag = rand()%4096;
            cache_line[idx][j].dirty = 0;
            cache_line[idx][j].rp_value = associativity-1-j;
            while (cache_line[idx][j].tag == tag) {
                cache_line[idx][j].tag = rand()%4096;
            }
        }
        if (debug_on) {
            for (int j =  0; j < associativity; j++){
                printf("Block: %d | RP Value %d | Current tag: %d | New tag %d\n", j, cache_line[idx][j].rp_value, cache_line[idx][j].tag , tag);
            }
            //printf("######################################################\n");
        }
        loadstore = k;
        status = lru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
        if (debug_on) {
            for (int j =  0; j < associativity; j++){
                printf("Block: %d | RP Value %d | Current tag: %d | New tag %d\n", j, cache_line[idx][j].rp_value, cache_line[idx][j].tag , tag);
            }
            printf("---------------------------------------------------------------\n\n");
        }
        EXPECT_EQ(status, 0);
        EXPECT_EQ(result.dirty_eviction, 0);
        expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
        EXPECT_EQ(result.miss_hit, expected_miss_hit);
    }
    /*
     * Check for hit: block was replaced in last iteration, if we used the same
     * tag now we will get a hit
     *
    DEBUG(\nChecking hit operation);
    for (int i = 0 ; i < 2; i++){
        loadstore = i;
        if (debug_on) {
            for (int l =  0; l < associativity; l++){
                printf("Block: %d | RP Value %d | Current tag: %d | Hit tag %d\n", l, cache_line[idx][l].rp_value, cache_line[idx][l].tag , tag);
            }
        }
        status = lru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
       if (debug_on) {
           for (int j =  0; j < associativity; j++){
               printf("Block: %d | RP Value %d | Current tag: %d | Hit tag %d\n", j, cache_line[idx][j].rp_value, cache_line[idx][j].tag , tag);
           }
           printf("---------------------------------------------------------------\n\n");
       }
        EXPECT_EQ(status, 0);
        EXPECT_EQ(result.dirty_eviction, 0);
        expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
        EXPECT_EQ(result.miss_hit, expected_miss_hit);
    }
}

/*
 * TEST3: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 *
TEST(L1cache, promotion){
  int associativity = 0, idx  = 0, tag = rand()%4096, address_a, i, j,status;
  bool loadstore  = true;
  enum replacement_policy rp;
  enum miss_hit_status expected_miss_hit;
  struct operation_result result  = {};
  DEBUG(Choosing random policy);

  j = rand()%3;
  switch(j){// Se obtiene una política de reemplazo aleatoria.
    case 0: rp  = LRU;
    break;
    case 1: rp  = NRU;
    break;
    case 2: rp  = SRRIP;
    break;
  }


  if(rp ==  LRU){
    if(debug_on)printf("LRU replacement policy\n");
    DEBUG(Choosing random associativity);
    while(associativity==0){// Se obtiene una asociatividad aleatoria distinta de 0.
      associativity = 1 << (rand()%4);
    }
    if(debug_on) printf("Associativity: %d\n",associativity);

    DEBUG(Filling cache entry);
    struct entry cache_line[idx][associativity];
    for (i = 0 ; i < 2; i++){// Se realiza la prueba una vez para load y otra para store.
      for (j =  0; j < associativity; j++) {// Se llena la línea de caché con datos aleatorios.

        cache_line[idx][j].valid = true;
        cache_line[idx][j].tag = rand()%4096;
        cache_line[idx][j].dirty = 0;
        cache_line[idx][j].rp_value = j;// El rp_value se asigna ascendentemente para que no sean iguales.
        while (cache_line[idx][j].tag == tag) {// Si un tag aleatorio es igual al tag que se quiere usar, se cambia.
          cache_line[idx][j].tag = rand()%4096;
        }
        if(debug_on) printf("Tag in way %d: %d\n",j,cache_line[idx][j].tag);
        if(debug_on) printf("Rp value in way %d: %d\n",j,cache_line[idx][j].rp_value);
      }
      DEBUG(Inserting new block);
      // Se introduce el bloque en la línea con un miss.
      loadstore = (bool)i;
      status = lru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
      EXPECT_EQ(status, 0);
      expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
      j = 0;
      while(cache_line[idx][j].tag != tag){// Se busca en la línea donde se encuentra el bloque ingresado.
        j++;
      }
      if(debug_on) printf("Tag %d is in way %d\n",tag,j);
      EXPECT_EQ(cache_line[idx][j].rp_value, 0);// Se espera que el bloque ingresado tenga un rp_value de 0.

      DEBUG(Forcing hit);
      // Se realiza un hit al bloque ingresado anteriormente.
      status = lru_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
      EXPECT_EQ(status, 0);
      expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
      EXPECT_EQ(result.miss_hit, expected_miss_hit);
      EXPECT_EQ(cache_line[idx][j].rp_value, 0);// Se espera que el rp_value siga siendo 0.

      DEBUG(Inserting new blocks);
      j = 0;
      address_a = combine(tag,idx);// address del bloque que se va a reemplazar.
      while(result.evicted_address!=address_a){// Se ingresan nuevos bloques hasta que A salga.
        tag = rand()%4096;// Tag de los bloques aleatorios.
        status = lru_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        j++;
        if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
      }
      EXPECT_EQ(result.evicted_address,address_a);
      if(debug_on)printf("Block evicted after %d new blocks were inserted\n",j);

    }
  }
  if(rp ==  NRU){
    if(debug_on) printf("NRU replacement policy\n");
      DEBUG(Choosing random associativity);
      while(associativity>2 || associativity==0){// Se obtiene una asociatividad aleatoria (1 o 2).
        associativity = 1 << (rand()%4);
      }
    if(debug_on) printf("Associativity: %d\n",associativity);


    DEBUG(Filling cache entry);
    struct entry cache_line[idx][associativity];
    for (i = 0 ; i < 2; i++){// Se itera dos veces (load y store).
      for (j =  0; j < associativity; j++) {

        cache_line[idx][j].valid = true;
        cache_line[idx][j].tag = rand()%4096;
        cache_line[idx][j].dirty = 0;
        cache_line[idx][j].rp_value = rand()%2;// Los rp_value son aleatorios (0 o 1.)
        while (cache_line[idx][j].tag == tag) {// Si un tag aleatorio es igual al tag que se quiere usar, se cambia.
          cache_line[idx][j].tag = rand()%4096;
        }
        if(debug_on) printf("Tag in way %d: %d\n",j,cache_line[idx][j].tag);
        if(debug_on) printf("Rp value in way %d: %d\n",j,cache_line[idx][j].rp_value);
    }
      DEBUG(Inserting new block);
      //Se ingresa el nuevo bloque.
      loadstore = (bool)i;
      status = srrip_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
      EXPECT_EQ(status, 0);
      expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;// Se espera obtener un miss.
      j = 0;
      while(cache_line[idx][j].tag != tag){// Se busca la posición del bloque ingresado.
        j++;
      }
      if(debug_on) printf("Tag %d is in way %d\n",tag,j);
      EXPECT_EQ(cache_line[idx][j].rp_value, 0);// Se verifica que el rp_value sea 0.

      // Luego de realizar un hit se espera que el rp_value sea 0.
      DEBUG(Forcing hit);
      status = srrip_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
      EXPECT_EQ(status, 0);
      expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
      EXPECT_EQ(result.miss_hit, expected_miss_hit);
      EXPECT_EQ(cache_line[idx][j].rp_value, 0);
      DEBUG(Inserting new blocks);
      j = 0;
      address_a = combine(tag,idx);
      while(result.evicted_address!=address_a){// Se ingresan bloques hasta sacar el bloque ingresado inicialmente.
        tag = rand()%4096;
        status = srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        j++;
        if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
      }
      EXPECT_EQ(result.evicted_address,address_a);
      if(debug_on)printf("Block evicted after %d new blocks were inserted\n",j);



    }
  }
  if(rp ==  SRRIP){
    if(debug_on) printf("SSRRIP replacement policy\n");
      DEBUG(Choosing random associativity);
      while(associativity<=2){// Se busca una asociatividad mayor a 2.
        associativity = 1 << (rand()%4);
      }
    if(debug_on) printf("Associativity: %d\n",associativity);


    DEBUG(Filling cache entry);
    struct entry cache_line[idx][associativity];
    for (i = 0 ; i < 2; i++){
      for (j =  0; j < associativity; j++) {

        cache_line[idx][j].valid = true;
        cache_line[idx][j].tag = rand()%4096;
        cache_line[idx][j].dirty = 0;
        cache_line[idx][j].rp_value = rand()%4;// Se asigna un rp_value aleatorio entre 0 y 3.
        while (cache_line[idx][j].tag == tag) {// Se verifica que ningún tag coincida con el que se desea ingresar.
          cache_line[idx][j].tag = rand()%4096;
        }
        if(debug_on) printf("Tag in way %d: %d\n",j,cache_line[idx][j].tag);
        if(debug_on) printf("Rp value in way %d: %d\n",j,cache_line[idx][j].rp_value);
      }


      DEBUG(Inserting new block);
      //Se ingresa el nuevo bloque y se espera un miss.
      loadstore = (bool)i;
      status = srrip_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
      EXPECT_EQ(status, 0);
      expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
      j = 0;
      while(cache_line[idx][j].tag != tag){// Se busca en la línea la posición del bloque ingresado.
        j++;
      }
      if(debug_on) printf("Tag %d is in way %d\n",tag,j);
      EXPECT_EQ(cache_line[idx][j].rp_value, 2);// Se verifica que el bloque ingresado tenga rp_value 2.

      DEBUG(Forcing hit);
      status = srrip_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       loadstore,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
      EXPECT_EQ(status, 0);
      expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
      EXPECT_EQ(result.miss_hit, expected_miss_hit);
      EXPECT_EQ(cache_line[idx][j].rp_value, 0);// Se verifica que el rp_value sea 0.

      DEBUG(Inserting new blocks);
      j = 0;
      address_a = combine(tag,idx);
      // Se ingresan nuevos bloques hasta que el bloque ingresado inicialmente salga.
      while(result.evicted_address!=address_a){
        tag = rand()%4096;
        status = srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         loadstore,
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        j++;
        if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
      }
      EXPECT_EQ(result.evicted_address,address_a);
      if(debug_on)printf("Block evicted after %d new blocks were inserted\n",j);
    }


  }

}


/*
 * TEST4: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 *
TEST(L1cache, writeback){
  int associativity = 0, idx  = 0,tag, tag_a = rand()%4096,tag_b = rand()%4096,i, j,status,address_a,address_b;
  bool a_eviction  = false;
  enum replacement_policy rp;
  enum miss_hit_status expected_miss_hit;
  struct operation_result result  = {};
  DEBUG(Choosing random policy);

  j = rand()%3; // Se obtiene una asociatividad aleatoria.
  switch(j){
    case 0: rp  = LRU;
    break;
    case 1: rp  = NRU;
    break;
    case 2: rp  = SRRIP;
    break;
  }
  if(rp==LRU){
    if(debug_on)printf("LRU replacement policy\n");
    DEBUG(Choosing random associativity);
    // Se busca una asociatividad mayor que 1, ya que se deben tener dos bloques distintos en la línea.
    while(associativity<=1){
      associativity = 1 << (rand()%4);
    }
    if(debug_on) printf("Associativity: %d\n",associativity);

    DEBUG(Filling cache entry);
    struct entry cache_line[idx][associativity];
      for (j =  0; j < associativity; j++) {

        cache_line[idx][j].valid = true;
        cache_line[idx][j].tag = rand()%4096;
        cache_line[idx][j].dirty = 0;// Se ingresan bloques con dirty en 0 (load).
        cache_line[idx][j].rp_value = j;// Se asignan rp_value distintos a cada bloque.
        }
        // Se eligen las primeros dos bloques para realizar la prueba.
        tag_a = cache_line[idx][1].tag;
        tag_b = cache_line[idx][0].tag;
        if(debug_on)printf("Tag A:%d\n",tag_a);
        if(debug_on)printf("Tag B:%d\n",tag_b);

        DEBUG(Forcing write hit for block A);
        // Se realiza un hit del bloque A.
        status = lru_replacement_policy(idx,
                                         tag_a,
                                         associativity,
                                         true,// La operación es un store.
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        EXPECT_EQ(result.miss_hit, HIT_STORE);
        // Se realiza un hit del bloque B.
        DEBUG(Forcing read hit for block B);
        status = lru_replacement_policy(idx,
                                         tag_b,
                                         associativity,
                                         false,// La operación es un load.
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        EXPECT_EQ(result.miss_hit, HIT_LOAD);

        // Se HIT_LOAD en el bloque A.
        DEBUG(Forcing read hit for block A);
        status = lru_replacement_policy(idx,
                                         tag_a,
                                         associativity,
                                         false,
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        EXPECT_EQ(result.miss_hit, HIT_LOAD);

        // Se insertan nuevos bloques hasta que B salga.
        DEBUG(Inserting new blocks for block B eviction);
        j = 0;
        address_b = combine(tag_b,idx);// Address del bloque B.
        address_a = combine(tag_a,idx);// Address del bloque A.
        while(result.evicted_address!=address_b){
          tag = rand()%4096;
          status = lru_replacement_policy(idx,
                                           tag,
                                           associativity,
                                           false,//Operación load para no cambiar el dirty.
                                           cache_line[0],
                                           &result,
                                           (bool)debug_on);
          EXPECT_EQ(status, 0);
          j++;
          if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
          // Si el bloque A fue reemplazado antes que el bloque B se analiza este bloque primero.
          if(result.evicted_address==address_a){
            DEBUG(Block A was evicted before block B);
            EXPECT_EQ(result.evicted_address,address_a);
            EXPECT_EQ(result.dirty_eviction,true);// Se verifica que el dirty sea true, debido a la operación store.
            a_eviction=true;// Se indica que A fue reemplazado antes que B.
          }
        }
        //Cuando B es reemplazado se verifica si el dirty es false.
        EXPECT_EQ(result.evicted_address,address_b);
        EXPECT_EQ(result.dirty_eviction,false);
        if(a_eviction==false){// Si A no ha sido reemplazado entonces se ingresan bloques hasta sacarlo.
          DEBUG(Inserting new blocks for block A eviction);
          j = 0;
          while(result.evicted_address!=address_a){
            tag = rand()%4096;
            status = lru_replacement_policy(idx,
                                             tag,
                                             associativity,
                                             false,
                                             cache_line[0],
                                             &result,
                                             (bool)debug_on);
            EXPECT_EQ(status, 0);
            j++;
            if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
          }
          //Cuando A sale se verifica que el dirty sea true por la operación store.
          EXPECT_EQ(result.evicted_address,address_a);
          EXPECT_EQ(result.dirty_eviction,true);
        }

      }

  if(rp==NRU){
    if(debug_on)printf("NRU replacement policy\n");
    // Asociatividad es 2 ya que se necesita que sea distinta de 0 y mayor que 1.
    associativity = 2;
    DEBUG(Filling cache entry);
    struct entry cache_line[idx][associativity];
    for (j =  0; j < associativity; j++) {

      cache_line[idx][j].valid = true;
      cache_line[idx][j].tag = rand()%4096;
      cache_line[idx][j].dirty = 0;//Dirty en 0 (load).
      cache_line[idx][j].rp_value = rand()%2; //rp_value aleatorios (0 o 1).
      }

    //A partir de aquí el procedimiento es similar al de LRU en el test writeback, pero con la función srrip_replacement_policy().
    tag_a = cache_line[idx][0].tag;
    tag_b = cache_line[idx][1].tag;
    if(debug_on)printf("Tag A:%d\n",tag_a);
    if(debug_on)printf("Tag B:%d\n",tag_b);

    DEBUG(Forcing write hit for block A);
    status = srrip_replacement_policy(idx,
                                     tag_a,
                                     associativity,
                                     true,
                                     cache_line[0],
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.miss_hit, HIT_STORE);

    DEBUG(Forcing read hit for block B);
    status = srrip_replacement_policy(idx,
                                     tag_b,
                                     associativity,
                                     false,
                                     cache_line[0],
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.miss_hit, HIT_LOAD);

    DEBUG(Forcing read hit for block A);
    status = srrip_replacement_policy(idx,
                                     tag_a,
                                     associativity,
                                     false,
                                     cache_line[0],
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.miss_hit, HIT_LOAD);

    DEBUG(Inserting new blocks for block B eviction);
    j = 0;
    address_b = combine(tag_b,idx);
    address_a = combine(tag_a,idx);
    while(result.evicted_address!=address_b){
      tag = rand()%4096;
      status = srrip_replacement_policy(idx,
                                       tag,
                                       associativity,
                                       false,
                                       cache_line[0],
                                       &result,
                                       (bool)debug_on);
      EXPECT_EQ(status, 0);
      j++;
      if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
      if(result.evicted_address==address_a){
        DEBUG(Block A was evicted before block B);
        EXPECT_EQ(result.evicted_address,address_a);
        EXPECT_EQ(result.dirty_eviction,true);
        a_eviction=true;
      }
    }
    EXPECT_EQ(result.evicted_address,address_b);
    EXPECT_EQ(result.dirty_eviction,false);
    if(a_eviction==false){
      DEBUG(Inserting new blocks for block A eviction);
      j = 0;
      while(result.evicted_address!=address_a){
        tag = rand()%4096;
        status = srrip_replacement_policy(idx,
                                         tag,
                                         associativity,
                                         false,
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        j++;
        if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
      }
      EXPECT_EQ(result.evicted_address,address_a);
      EXPECT_EQ(result.dirty_eviction,true);
    }

  }
  if(rp==SRRIP){

      if(debug_on) printf("SSRRIP replacement policy\n");
        DEBUG(Choosing random associativity);
        while(associativity<=2){// Se busca una asociatividad mayor que 2.
          associativity = 1 << (rand()%4);
        }
      if(debug_on) printf("Associativity: %d\n",associativity);

      DEBUG(Filling cache entry);
      struct entry cache_line[idx][associativity];
        for (j =  0; j < associativity; j++) {

          cache_line[idx][j].valid = true;
          cache_line[idx][j].tag = rand()%4096;
          cache_line[idx][j].dirty = 0;//dirty de operaciones load.
          cache_line[idx][j].rp_value = rand()%4;// rp_value entre 0 y 3.
        }
        // A partir de aquí el procedimiento es el mismo que los anteriores para el test writeback.
        tag_a = cache_line[idx][1].tag;
        tag_b = cache_line[idx][0].tag;
        if(debug_on)printf("Tag A:%d\n",tag_a);
        if(debug_on)printf("Tag B:%d\n",tag_b);

        DEBUG(Forcing write hit for block A);
        status = srrip_replacement_policy(idx,
                                         tag_a,
                                         associativity,
                                         true,
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        EXPECT_EQ(result.miss_hit, HIT_STORE);

        DEBUG(Forcing read hit for block B);
        status = srrip_replacement_policy(idx,
                                         tag_b,
                                         associativity,
                                         false,
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        EXPECT_EQ(result.miss_hit, HIT_LOAD);

        DEBUG(Forcing read hit for block A);
        status = srrip_replacement_policy(idx,
                                         tag_a,
                                         associativity,
                                         false,
                                         cache_line[0],
                                         &result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        EXPECT_EQ(result.miss_hit, HIT_LOAD);

        DEBUG(Inserting new blocks for block B eviction);
        j = 0;
        address_b = combine(tag_b,idx);
        address_a = combine(tag_a,idx);
        while(result.evicted_address!=address_b){
          tag = rand()%4096;
          status = srrip_replacement_policy(idx,
                                           tag,
                                           associativity,
                                           false,
                                           cache_line[0],
                                           &result,
                                           (bool)debug_on);
          EXPECT_EQ(status, 0);
          j++;
          if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
          if(result.evicted_address==address_a){
            DEBUG(Block A was evicted before block B);
            EXPECT_EQ(result.evicted_address,address_a);
            EXPECT_EQ(result.dirty_eviction,true);
            a_eviction=true;
          }
        }
        EXPECT_EQ(result.evicted_address,address_b);
        EXPECT_EQ(result.dirty_eviction,false);
        if(a_eviction==false){
          DEBUG(Inserting new blocks for block A eviction);
          j = 0;
          while(result.evicted_address!=address_a){
            tag = rand()%4096;
            status = srrip_replacement_policy(idx,
                                             tag,
                                             associativity,
                                             false,
                                             cache_line[0],
                                             &result,
                                             (bool)debug_on);
            EXPECT_EQ(status, 0);
            j++;
            if(debug_on) printf("Evicted address: %d\n",result.evicted_address);
          }
          EXPECT_EQ(result.evicted_address,address_a);
          EXPECT_EQ(result.dirty_eviction,true);


  }

}
}

/*
 * TEST5: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy
 * 2. Choose invalid parameters for cache_size, block_size and associativity
 * 3. Check function returns a PARAM error condition
 *
TEST(L1cache, boundaries){

    int status = 0;
    int expected_status = 0;
    int associativity = 0;
    int replacement_policy = 0;
    int cachesize_kb = 0;
    int blocksize_bytes = 0;
    int *fields;

    /* Pick a random replacement policy *
    DEBUG(Picking random policy);
    replacement_policy = rand() % 5;
    if (replacement_policy > RANDOM) {
        expected_status = ERROR;
    } else {
        expected_status = OK;
    }
    if (debug_on) printf("Replacement policy value: %d\n", replacement_policy);
    status = replacement_policy_select( &replacement_policy );
    if (debug_on) printf("Replacement policy selected: %d\n", replacement_policy);
    EXPECT_EQ(status, expected_status);

    /* Pick invalid params for idx, tag and associativity*/
    /* Associativity case *
    DEBUG(Picking invalid associativity);
    cachesize_kb = 1 << (rand()%8);
    blocksize_bytes = 1 << (rand()%8);
    while (associativity > cachesize_kb*1024){
        associativity = (rand()%200+256);
    }

    if (debug_on) printf("\nCache size: %d | Block size: %d | Associativity: %d\n",cachesize_kb,  blocksize_bytes, associativity);
    status = field_size_get( cachesize_kb,
                        associativity,
                        blocksize_bytes,
                        fields,
                        fields,
                        fields);

    EXPECT_EQ(status, PARAM);

    /* Block size case *
    blocksize_bytes = 0;
    DEBUG(Picking invalid block_size);
    cachesize_kb = 1 << (rand()%8);
    associativity = 1 << (rand()%8);
    while (blocksize_bytes > cachesize_kb*1024){
        blocksize_bytes = (rand()%200+256);
    }

    if (debug_on) printf("\nCache size: %d | Block size: %d | Associativity: %d\n",cachesize_kb,  blocksize_bytes, associativity);
    status = field_size_get( cachesize_kb,
                        associativity,
                        blocksize_bytes,
                        fields,
                        fields,
                        fields);

    EXPECT_EQ(status, PARAM);

    /* Cache size case *
    DEBUG(Picking invalid cache size);
    blocksize_bytes = 1 << ((rand()%8) + 15);
    associativity = 1 << ((rand()%8) + 15);
    while (blocksize_bytes < cachesize_kb*1024 || associativity < cachesize_kb*1024){
        cachesize_kb = ((rand()%6) -3);
    }
    if (debug_on) printf("\nCache size: %d | Block size: %d | Associativity: %d\n",cachesize_kb,  blocksize_bytes, associativity);

    status = field_size_get( cachesize_kb,
                        associativity,
                        blocksize_bytes,
                        fields,
                        fields,
                        fields);

    EXPECT_EQ(status, PARAM);
}
TEST(Victimcache, hit_miss){
      int status;
      int tagA;
      int i;
      int j;
      int idx;
      int associativity;
      int victim_associativity=16;
      bool expected_dirty;
      int lru_l1;
      bool loadstore = 1;
      bool debug = 1;
      enum miss_hit_status expected_miss_hit;
      struct operation_result result = {};
      struct victim_result victim_result = {};


      tagA=rand()%4096;
      /* Fill a random cache entry *
      idx = rand()%1024;
      tagA = rand()%4096;
      associativity = 1 << (rand()%4);
      if (debug_on) {
        printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
              idx,
              tagA,
              associativity);
      }

      struct entry cache_line[idx][associativity];
      struct victim_cache victim[victim_associativity];
      /* Check for a miss *
      DEBUG(Checking L1 miss and VC hit);
      for (i = 0 ; i < 2; i++){
        /* Fill victim cache *

        for ( j =  0; j < victim_associativity; j++) {
            victim[j].valid = true;
            victim[j].address = rand()%4096;
            victim[j].rp_value = j;
          while (victim[j].address == combine(tagA,idx)) {
            victim[j].address = rand()%4096;
          }
        }
        victim[15].address= combine(tagA,idx);
        /* Fill cache line *
        for ( j =  0; j < associativity; j++) {
          cache_line[idx][j].valid = true;
          cache_line[idx][j].tag = rand()%4096;
          cache_line[idx][j].dirty = 0;
          cache_line[idx][j].rp_value = associativity -1 -j;
          while (cache_line[idx][j].tag == tagA) {
            cache_line[idx][j].tag = rand()%4096;
          }
        }
        lru_l1 = cache_line[idx][0].tag;
        loadstore = (bool)i;
        status = victim_opt(idx,
                                         tagA,
                                         associativity,
                                         victim_associativity,
                                         loadstore,
                                         cache_line[0],
                                         victim,
                                         &result,
                                         &victim_result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
        EXPECT_EQ(result.miss_hit,expected_miss_hit);
        EXPECT_EQ(victim_result.miss_hit,HITV);
        EXPECT_EQ(cache_line[idx][0].tag,tagA);
        EXPECT_EQ(victim[15].address,combine(lru_l1,idx));
        EXPECT_EQ(cache_line[idx][0].rp_value,0);
        expected_dirty = loadstore ? true: false;
        EXPECT_EQ(cache_line[idx][0].dirty, expected_dirty);
         }
    }

TEST(Victimcache, miss_miss){
      int status;
      int tagA;
      int i;
      int j;
      int idx;
      int associativity;
      int victim_associativity=16;
      bool expected_dirty;
      int lru_l1;
      bool loadstore = 1;
      bool debug = 1;
      int lru_victim_evicted_address;
      enum miss_hit_status expected_miss_hit;
      struct operation_result result = {};
      struct victim_result victim_result = {};


      tagA=rand()%4096;
      /* Fill a random cache entry *
      idx = rand()%1024;
      tagA = rand()%4096;
      //associativity = 1 << (rand()%4);
      associativity=2;
      if (debug_on) {
        printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
              idx,
              tagA,
              associativity);
      }

      struct entry cache_line[idx][associativity];
      struct victim_cache victim[victim_associativity];
      /* Check for a miss *
      DEBUG(Checking L1 miss and VC hit);
      for (i = 0 ; i < 2; i++){
        /* Fill victim cache *

        for ( j =  0; j < victim_associativity; j++) {
            victim[j].valid = true;
            victim[j].address = rand()%4096;
            victim[j].rp_value = j;
          while (victim[j].address == combine(tagA,idx)) {
            victim[j].address = rand()%4096;
          }
        }
        /* Fill cache line *
        for ( j =  0; j < associativity; j++) {
          cache_line[idx][j].valid = true;
          cache_line[idx][j].tag = rand()%4096;
          cache_line[idx][j].dirty = 0;
          cache_line[idx][j].rp_value = associativity -1 -j;
          while (cache_line[idx][j].tag == tagA) {
            cache_line[idx][j].tag = rand()%4096;
          }
        }
        lru_l1 = cache_line[idx][0].tag;
        lru_victim_evicted_address=victim[15].address;
        loadstore = (bool)i;
        status = victim_opt(idx,
                                         tagA,
                                         associativity,
                                         victim_associativity,
                                         loadstore,
                                         cache_line[0],
                                         victim,
                                         &result,
                                         &victim_result,
                                         (bool)debug_on);
        EXPECT_EQ(status, 0);
        expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
        EXPECT_EQ(result.miss_hit,expected_miss_hit);
        EXPECT_EQ(victim_result.miss_hit,MISSV);
        EXPECT_EQ(victim[15].address,combine(lru_l1,idx));
        EXPECT_EQ(victim_result.evicted_address,lru_victim_evicted_address);
        EXPECT_EQ(cache_line[idx][0].rp_value,0);
        expected_dirty = loadstore ? true: false;
        EXPECT_EQ(cache_line[idx][0].dirty, expected_dirty);
         }
}*/

TEST(L2cache, hit_miss_multilevel_cache) {
    int status = 0;
    int L1_associativity = 0;
    int L2_associativity = 0;
    int L1_tag_fixed = 0;
    int L2_tag_fixed = 0;
    long address = 0;
    int *L1_tag_size=(int *)malloc(sizeof (int));
    int *L1_idx_size=(int *)malloc(sizeof (int));
    int *L1_offset_size=(int *)malloc(sizeof (int));
    int *L2_tag_size=(int *)malloc(sizeof (int));
    int *L2_idx_size=(int *)malloc(sizeof (int));
    int *L2_offset_size=(int *)malloc(sizeof (int));

    int *L1_idx=(int *)malloc(sizeof (int));
    int *L2_idx=(int *)malloc(sizeof (int));
    int *L2_idx_ignore=(int *)malloc(sizeof (int));


    int *L1_tag=(int *)malloc(sizeof (int));
    int *L2_tag=(int *)malloc(sizeof (int));

    enum miss_hit L1_expected_miss_hit = MISS;
    enum miss_hit L2_expected_miss_hit = MISS;

    int loadstore = 0;
    bool debug = debug_on;

    struct result L1_result = {};
    struct result L2_result = {};
    struct operation_result L2_result_tmp={};

    /* Fill a random cache entry */
    L1_associativity = 1 << (rand()%3);
    L2_associativity = 2 * L1_associativity;

    multilevel_field_size_get(64, L1_associativity, 16, L1_tag_size, L2_tag_size, L1_idx_size, L2_idx_size, L1_offset_size, L2_offset_size);

    *L1_idx = rand()%(1<<*L1_idx_size); // fixed index
    L1_tag_fixed = rand()%(1<<*L1_tag_size); //hit tag
    address_tag_idx_get(combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx, &L2_tag_fixed);

    struct entry L1_cache[*L1_idx][L1_associativity];
    struct entry L2_cache[*L2_idx][L2_associativity];


    if(debug){
        printf("L1 dimensions: Tag: %d Idx: %d Offset: %d Asoc: %d \n", *L1_tag_size, *L1_idx_size, *L1_offset_size, L1_associativity);
        printf("L2 dimensions: Tag: %d Idx: %d Offset: %d Asoc: %d \n", *L2_tag_size, *L2_idx_size, *L1_offset_size, L2_associativity);
        printf("L1_idx: %d L2_idx: %d \n", *L1_idx, *L2_idx);
        printf("L1_fixed_tag: %d L2_fixed_tag: %d \n", L1_tag_fixed, L2_tag_fixed);
    }

    if (debug) printf("\nL1_address: %ld\n", combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size));
    if (debug) printf("L2_address: %ld\n", combine_address(*L2_idx, L2_tag_fixed, *L2_idx_size, *L2_offset_size));

    DEBUG(\nFilling both caches);
    for (int j = 0; j < L2_associativity; j++){ // fills a cache line.
        *L1_tag = rand()%(1<<*L1_tag_size);
        while(L1_tag_fixed == *L1_tag){
            *L1_tag = rand()%(1<<*L1_tag_size);
        }
        address_tag_idx_get(combine_address(*L1_idx, *L1_tag, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx_ignore, L2_tag);
        multilevel_lru_replacement(*L1_idx, *L2_idx, *L1_tag, *L2_tag, L1_associativity, L2_associativity, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, L1_cache[0], L2_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

        if (debug){
            printf("\nL1 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d Rp: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].rp_value);
            }
            printf("\nL2 cache: \n");
            for(int i = 0; i < L2_associativity; i++){
                printf("Entry: %d Tag: %d Rp: %d | ", i, L2_cache[*L2_idx][i].tag, L2_cache[*L2_idx][i].rp_value);
            }
            printf("\n");
        }
    }


        if (debug) printf("\nL1_address: %ld\n", combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size));
        if (debug) printf("L2_address: %ld\n", combine_address(*L2_idx, L2_tag_fixed, *L2_idx_size, *L2_offset_size));

    if (debug){
        printf("\nL1 cache: \n");
        for(int i = 0; i < L1_associativity; i++){
            printf("Entry: %d Tag: %d | ", i, L1_cache[*L1_idx][i].tag);
        }
        printf("\nL2 cache: \n");
        for(int i = 0; i < L2_associativity; i++){
            printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
        }
        printf("\n");
    }

    /* Check for a miss - miss */
    DEBUG(\nChecking miss-miss operation);

    if (debug) printf("L1_idx: %d L2_idx: %d \n", *L1_idx, *L2_idx);

    status = multilevel_lru_replacement(*L1_idx, *L2_idx, L1_tag_fixed, L2_tag_fixed, L1_associativity, L2_associativity, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, L1_cache[0], L2_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

    if (debug) printf("\nAfter Addresses: \nL1_address: %ld\n", L1_result.evicted_address);
    if (debug) printf("L2_address: %ld\n", L2_result.evicted_address);

    if (debug) printf("Evicted Address (outside): %ld\n", L1_result.evicted_address);
    if (debug){
        printf("\nL1 cache: \n");
        for(int i = 0; i < L1_associativity; i++){
            printf("Entry: %d Tag: %d Rp: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].rp_value);
        }
        printf("\nL2 cache: \n");
        for(int i = 0; i < L2_associativity; i++){
            printf("Entry: %d Tag: %d Rp: %d | ", i, L2_cache[*L2_idx][i].tag, L2_cache[*L2_idx][i].rp_value);
        }
        printf("\n");
    }

    if (L2_result.evicted_address) status = OK;

    EXPECT_EQ(status, OK);
    EXPECT_EQ(L1_result.miss_hit, MISS);
    EXPECT_EQ(L2_result.miss_hit, MISS);

    /* Check for a hit - hit */
    DEBUG(\nChecking hit-hit operation);
    /*
     * Check for hit: block was replaced in last iteration, if we used the same
     * tag now we will get a hit
     */

    status = multilevel_lru_replacement(*L1_idx, *L2_idx, L1_tag_fixed, L2_tag_fixed, L1_associativity, L2_associativity, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, L1_cache[0], L2_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

    EXPECT_EQ(status, 0);
    EXPECT_EQ(L1_result.miss_hit, HIT);
    EXPECT_EQ(L2_result.miss_hit, HIT);

    /* Check for a miss - miss */
    DEBUG(\nChecking miss-hit operation);

    for (int j = 0; j < L1_associativity; j++){ // re-fills a cache line.
        *L1_tag = rand()%(1<<*L1_tag_size);
        while(L1_tag_fixed == *L1_tag){
            *L1_tag = rand()%(1<<*L1_tag_size);
        }
        address_tag_idx_get(combine_address(*L1_idx, *L1_tag, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx_ignore, L2_tag);

        multilevel_lru_replacement(*L1_idx, *L2_idx, *L1_tag, *L2_tag, L1_associativity, L2_associativity, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, L1_cache[0], L2_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);
    }

    if (debug) {
        for (int j = 0; j < L1_associativity; j++){
            printf("L1 info: Way: %d Tag: %d\n", j, L1_cache[*L1_idx][j].tag);
        }
    }

    status = multilevel_lru_replacement(*L1_idx, *L2_idx, L1_tag_fixed, L2_tag_fixed, L1_associativity, L2_associativity, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, L1_cache[0], L2_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

    if (L1_result.evicted_address) status = OK;

    EXPECT_EQ(status, OK);
    EXPECT_EQ(L1_result.miss_hit, MISS);
    EXPECT_EQ(L2_result.miss_hit, HIT);

    free(L1_tag_size);
    free(L1_idx_size);
    free(L1_offset_size);
    free(L2_tag_size);
    free(L2_idx_size);
    free(L2_offset_size);

    free(L1_idx);
    free(L2_idx);
    free(L2_idx_ignore);

    free(L1_tag);
    free(L2_tag);
}

/*
 * TEST: Verifies MSI coherence protocol values
 * 1. Choose random tag and idx
 * 2. Fill the three caches
 * 3. Force a write acces from CPU1 to invalidate CPU2 access
 * 4. Check cp values: MODIFIED for CPU1 and INVALID for CPU2. Check invalidation.
 * 5. Force a read acces from CPU1
 * 6. Check cp values: SHARED for CPU1 and INVALID for CPU2
 * 7. Force a read acces from CPU2. Data is shared with CPU1.
 * 8. Check cp values: SHARED for CPU1 and SHARED for CPU2
 * 9. Force a write acces from CPU2 to invalidate CPU1 access
 * 10. Check cp values: INVALID for CPU1 and MODIFIED for CPU2. Check invalidation.
 */

TEST(coherence, MSI_protocol_test) {
    // Various initializations
    int status = 0;
    int L1_associativity = 0;
    int L2_associativity = 0;
    int L1_tag_fixed = 0;
    int L2_tag_fixed = 0;
    long address = 0;
    bool invalidation = false;
    int *L1_tag_size=(int *)malloc(sizeof (int));
    int *L1_idx_size=(int *)malloc(sizeof (int));
    int *L1_offset_size=(int *)malloc(sizeof (int));
    int *L2_tag_size=(int *)malloc(sizeof (int));
    int *L2_idx_size=(int *)malloc(sizeof (int));
    int *L2_offset_size=(int *)malloc(sizeof (int));

    int *L1_idx=(int *)malloc(sizeof (int));
    int *L2_idx=(int *)malloc(sizeof (int));
    int *L2_idx_ignore=(int *)malloc(sizeof (int));


    int *L1_tag=(int *)malloc(sizeof (int));
    int *L2_tag=(int *)malloc(sizeof (int));
    int *L3_tag=(int *)malloc(sizeof (int));

    enum coherence_values L1_expected_coherence = INVALID;
    enum coherence_values L3_expected_coherence = INVALID;

    int loadstore = 0;
    bool debug = debug_on;

    struct result L1_result = {};
    struct result L2_result = {};
    struct result L3_result = {};
    struct operation_result L2_result_tmp={};

    // Choose random associativity
    L1_associativity = 1 << (rand()%2);
    L2_associativity = 2 * L1_associativity;

    //Gets sizes for caches
    multilevel_field_size_get(64, L1_associativity, 16, L1_tag_size, L2_tag_size, L1_idx_size, L2_idx_size, L1_offset_size, L2_offset_size);

    *L1_idx = rand()%(1<<*L1_idx_size); // fixed index
    L1_tag_fixed = rand()%(1<<*L1_tag_size); //fixed tag
    // Gets tag and idx for L2 from L1 fixed tags and idx
    address_tag_idx_get(combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx, &L2_tag_fixed);

    // Instantiates caches
    struct entry L1_cache[*L1_idx][L1_associativity];
    struct entry L2_cache[*L2_idx][L2_associativity];
    struct entry L3_cache[*L1_idx][L1_associativity];


    if(debug){
        printf("L1|L3 dimensions: Tag: %d Idx: %d Offset: %d Asoc: %d \n", *L1_tag_size, *L1_idx_size, *L1_offset_size, L1_associativity);
        printf("L2 dimensions: Tag: %d Idx: %d Offset: %d Asoc: %d \n", *L2_tag_size, *L2_idx_size, *L1_offset_size, L2_associativity);
        printf("L1_idx: %d L2_idx: %d \n", *L1_idx, *L2_idx);
        printf("L1_fixed_tag: %d L2_fixed_tag: %d \n", L1_tag_fixed, L2_tag_fixed);
    }

    if (debug) printf("\nL1|L3_address: %ld\n", combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size));
    if (debug) printf("L2_address: %ld\n", combine_address(*L2_idx, L2_tag_fixed, *L2_idx_size, *L2_offset_size));

    DEBUG(\nFilling caches);
    for (int j = 0; j < L1_associativity; j++){
        // Randomizes tags
        *L1_tag = rand()%(1<<*L1_tag_size);
        while(L1_tag_fixed == *L1_tag){
            *L1_tag = rand()%(1<<*L1_tag_size);
        }
        *L3_tag = rand()%(1<<*L1_tag_size);
        while(L1_tag_fixed == *L3_tag){
            *L3_tag = rand()%(1<<*L1_tag_size);
        }
        if (debug) printf("L1 tag: %d | L3 tag: %d\n", *L1_tag, *L3_tag);

        // Inserts in L1
        address_tag_idx_get(combine_address(*L1_idx, *L1_tag, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx_ignore, L2_tag);
        MSI_replacement( *L1_idx, *L2_idx, *L1_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

        if (debug){
            printf("\nL1 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
            }
            printf("\nL2 cache: \n");
            for(int i = 0; i < L2_associativity; i++){
                printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
            }
            printf("\nL3 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
            }
            printf("\n");
        }

        // Inserts in L3
        address_tag_idx_get(combine_address(*L1_idx, *L3_tag, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx_ignore, L2_tag);
        MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L3_cache[0], L2_cache[0], L1_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

        if (debug){
            printf("\nL1 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
            }
            printf("\nL2 cache: \n");
            for(int i = 0; i < L2_associativity; i++){
                printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
            }
            printf("\nL3 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
            }
            printf("\n");
        }
    }


        if (debug) printf("\nL1|L3_address: %ld\n", combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size));
        if (debug) printf("L2_address: %ld\n", combine_address(*L2_idx, L2_tag_fixed, *L2_idx_size, *L2_offset_size));


    if (debug) printf("\n#############################" );

    DEBUG(\nChecking CPU1 operations);
    for (int l = 0; l < 2; l++) {
        loadstore = 1-l; // Uses this logic to write first
        if (loadstore) {
            DEBUG(\nChecking write access form CPU1 invalidating CPU2);
            status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug); // Writes in L1

            if (debug){
                printf("\nL1 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
                }
                printf("\nL2 cache: \n");
                for(int i = 0; i < L2_associativity; i++){
                    printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
                }
                printf("\nL3 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
                }
                printf("\n");
            }

            L1_expected_coherence = MODIFIED;
            int L1_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L1_cache[0], debug);
            int L3_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L3_cache[0], debug);
            L3_expected_coherence = INVALID;

            EXPECT_EQ(status, OK);
            EXPECT_EQ(L1_expected_coherence, L1_coherence);
            EXPECT_EQ(L3_expected_coherence, L3_coherence);
            EXPECT_EQ(invalidation, true); // Expects invalidation from writing
        } else {
            DEBUG(\nChecking read access form CPU1);
            status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug); // Reads for L1

            if (debug){
                printf("\nL1 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
                }
                printf("\nL2 cache: \n");
                for(int i = 0; i < L2_associativity; i++){
                    printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
                }
                printf("\nL3 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
                }
                printf("\n");
            }

            L1_expected_coherence = SHARED;
            int L1_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L1_cache[0], debug);
            L3_expected_coherence = INVALID;

            EXPECT_EQ(status, OK);
            EXPECT_EQ(L1_expected_coherence, L1_coherence);
            EXPECT_EQ(invalidation, false);
        }
    }

    // Returns to CPU1: MODIFIED state and CPU2: INVALID state
    status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, 1/*writes*/, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

    DEBUG(\nChecking CPU2 operations);
    for (int l = 0; l < 2; l++) {
        loadstore = l; // reads first
        if (!loadstore) {
            DEBUG(\nChecking read access form CPU2);
            status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L3_cache[0], L2_cache[0], L1_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug); // Reads for L3

            if (debug){
                printf("\nL1 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
                }
                printf("\nL2 cache: \n");
                for(int i = 0; i < L2_associativity; i++){
                    printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
                }
                printf("\nL3 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
                }
                printf("\n");
            }

            L1_expected_coherence = SHARED;
            int L1_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L1_cache[0], debug);
            int L3_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L3_cache[0], debug);
            L3_expected_coherence = SHARED;

            EXPECT_EQ(status, OK);
            EXPECT_EQ(L1_expected_coherence, L1_coherence);
            EXPECT_EQ(L3_expected_coherence, L3_coherence);
            EXPECT_EQ(invalidation, false);
        } else {
            DEBUG(\nChecking write access form CPU2 invalidating CPU1);
            status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L3_cache[0], L2_cache[0], L1_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug); // Writes in L3

            if (debug){
                printf("\nL1 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
                }
                printf("\nL2 cache: \n");
                for(int i = 0; i < L2_associativity; i++){
                    printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
                }
                printf("\nL3 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
                }
                printf("\n");
            }

            L1_expected_coherence = INVALID;
            int L1_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L1_cache[0], debug);
            int L3_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L3_cache[0], debug);
            L3_expected_coherence = MODIFIED;

            EXPECT_EQ(status, OK);
            EXPECT_EQ(L1_expected_coherence, L1_coherence);
            EXPECT_EQ(L3_expected_coherence, L3_coherence);
            EXPECT_EQ(invalidation, true); // Expects invalidation from writing
        }
    }

    // Frees memory allocation
    free(L1_tag_size);
    free(L1_idx_size);
    free(L1_offset_size);
    free(L2_tag_size);
    free(L2_idx_size);
    free(L2_offset_size);
    free(L1_idx);
    free(L2_idx);
    free(L2_idx_ignore);
    free(L1_tag);
    free(L2_tag);
    free(L3_tag);
}

/*
 * TEST: Verifies MESI coherence protocol values
 * 1. Choose random tag and idx
 * 2. Fill the three caches
 * 3. Force a write acces from CPU1 to invalidate CPU2 access
 * 4. Check cp values: MODIFIED for CPU1 and INVALID for CPU2. Check invalidation.
 * 5. Force a read acces from CPU1
 * 6. Check cp values: SHARED for CPU1 and INVALID for CPU2
 * 7. Force a read acces from CPU2. Data is shared with CPU1.
 * 8. Check cp values: SHARED for CPU1 and SHARED for CPU2
 * 9. Force a write acces from CPU2 to invalidate CPU1 access
 * 10. Check cp values: INVALID for CPU1 and MODIFIED for CPU2. Check invalidation.
 */

TEST(coherence, MESI_protocol_test) {
    // Various initializations
    int status = 0;
    int L1_associativity = 0;
    int L2_associativity = 0;
    int L1_tag_fixed = 0;
    int L2_tag_fixed = 0;
    long address = 0;
    bool invalidation = false;
    int *L1_tag_size=(int *)malloc(sizeof (int));
    int *L1_idx_size=(int *)malloc(sizeof (int));
    int *L1_offset_size=(int *)malloc(sizeof (int));
    int *L2_tag_size=(int *)malloc(sizeof (int));
    int *L2_idx_size=(int *)malloc(sizeof (int));
    int *L2_offset_size=(int *)malloc(sizeof (int));

    int *L1_idx=(int *)malloc(sizeof (int));
    int *L2_idx=(int *)malloc(sizeof (int));
    int *L2_idx_ignore=(int *)malloc(sizeof (int));


    int *L1_tag=(int *)malloc(sizeof (int));
    int *L2_tag=(int *)malloc(sizeof (int));
    int *L3_tag=(int *)malloc(sizeof (int));

    enum coherence_values L1_expected_coherence = INVALID;
    enum coherence_values L3_expected_coherence = INVALID;

    int loadstore = 0;
    bool debug = debug_on;

    struct result L1_result = {};
    struct result L2_result = {};
    struct result L3_result = {};
    struct operation_result L2_result_tmp={};

    // Choose random associativity
    L1_associativity = 1 << (rand()%2);
    L2_associativity = 2 * L1_associativity;

    //Gets sizes for caches
    multilevel_field_size_get(64, L1_associativity, 16, L1_tag_size, L2_tag_size, L1_idx_size, L2_idx_size, L1_offset_size, L2_offset_size);

    *L1_idx = rand()%(1<<*L1_idx_size); // fixed index
    L1_tag_fixed = rand()%(1<<*L1_tag_size); //fixed tag
    // Gets tag and idx for L2 from L1 fixed tags and idx
    address_tag_idx_get(combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx, &L2_tag_fixed);

    // Instantiates caches
    struct entry L1_cache[*L1_idx][L1_associativity];
    struct entry L2_cache[*L2_idx][L2_associativity];
    struct entry L3_cache[*L1_idx][L1_associativity];


    if(debug){
        printf("L1|L3 dimensions: Tag: %d Idx: %d Offset: %d Asoc: %d \n", *L1_tag_size, *L1_idx_size, *L1_offset_size, L1_associativity);
        printf("L2 dimensions: Tag: %d Idx: %d Offset: %d Asoc: %d \n", *L2_tag_size, *L2_idx_size, *L1_offset_size, L2_associativity);
        printf("L1_idx: %d L2_idx: %d \n", *L1_idx, *L2_idx);
        printf("L1_fixed_tag: %d L2_fixed_tag: %d \n", L1_tag_fixed, L2_tag_fixed);
    }

    if (debug) printf("\nL1|L3_address: %ld\n", combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size));
    if (debug) printf("L2_address: %ld\n", combine_address(*L2_idx, L2_tag_fixed, *L2_idx_size, *L2_offset_size));

    DEBUG(\nFilling caches);
    for (int j = 0; j < L1_associativity; j++){
        // Randomizes tags
        *L1_tag = rand()%(1<<*L1_tag_size);
        while(L1_tag_fixed == *L1_tag){
            *L1_tag = rand()%(1<<*L1_tag_size);
        }
        *L3_tag = rand()%(1<<*L1_tag_size);
        while(L1_tag_fixed == *L3_tag){
            *L3_tag = rand()%(1<<*L1_tag_size);
        }
        if (debug) printf("L1 tag: %d | L3 tag: %d\n", *L1_tag, *L3_tag);

        // Inserts in L1
        address_tag_idx_get(combine_address(*L1_idx, *L1_tag, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx_ignore, L2_tag);
        MESI_replacement( *L1_idx, *L2_idx, *L1_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

        if (debug){
            printf("\nL1 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
            }
            printf("\nL2 cache: \n");
            for(int i = 0; i < L2_associativity; i++){
                printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
            }
            printf("\nL3 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
            }
            printf("\n");
        }

        // Inserts in L3
        address_tag_idx_get(combine_address(*L1_idx, *L3_tag, *L1_idx_size, *L1_offset_size), *L2_tag_size, *L2_idx_size, *L2_offset_size, L2_idx_ignore, L2_tag);
        MESI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L3_cache[0], L2_cache[0], L1_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

        if (debug){
            printf("\nL1 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
            }
            printf("\nL2 cache: \n");
            for(int i = 0; i < L2_associativity; i++){
                printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
            }
            printf("\nL3 cache: \n");
            for(int i = 0; i < L1_associativity; i++){
                printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
            }
            printf("\n");
        }
    }


        if (debug) printf("\nL1|L3_address: %ld\n", combine_address(*L1_idx, L1_tag_fixed, *L1_idx_size, *L1_offset_size));
        if (debug) printf("L2_address: %ld\n", combine_address(*L2_idx, L2_tag_fixed, *L2_idx_size, *L2_offset_size));


    if (debug) printf("\n#############################" );

    DEBUG(\nChecking CPU1 operations);

    DEBUG(\nChecking exclusive read access from CPU1 );
    MESI_replacement( *L1_idx, *L2_idx, L1_tag_fixed, L2_tag_fixed, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, 0 /*reads*/, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

    if (debug){
        printf("\nL1 cache: \n");
        for(int i = 0; i < L1_associativity; i++){
            printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
        }
        printf("\nL2 cache: \n");
        for(int i = 0; i < L2_associativity; i++){
            printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
        }
        printf("\nL3 cache: \n");
        for(int i = 0; i < L1_associativity; i++){
            printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
        }
        printf("\n");
    }

    L1_expected_coherence = EXCLUSIVE;
    int L1_coherence = check_cp(*L1_idx, L1_tag_fixed, L1_associativity, L1_cache[0], debug);
    int L3_coherence = check_cp(*L1_idx, L1_tag_fixed, L1_associativity, L3_cache[0], debug);
    L3_expected_coherence = INVALID;

    EXPECT_EQ(status, OK);
    EXPECT_EQ(L1_expected_coherence, L1_coherence);
    EXPECT_EQ(L3_expected_coherence, L3_coherence);

    DEBUG(\nChecking write access for CPU1 from exclusive read );

    MESI_replacement( *L1_idx, *L2_idx, L1_tag_fixed, L2_tag_fixed, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, 1 /*writes*/, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

    if (debug){
        printf("\nL1 cache: \n");
        for(int i = 0; i < L1_associativity; i++){
            printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
        }
        printf("\nL2 cache: \n");
        for(int i = 0; i < L2_associativity; i++){
            printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
        }
        printf("\nL3 cache: \n");
        for(int i = 0; i < L1_associativity; i++){
            printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
        }
        printf("\n");
    }

    L1_expected_coherence = MODIFIED;
    L1_coherence = check_cp(*L1_idx, L1_tag_fixed, L1_associativity, L1_cache[0], debug);
    L3_coherence = check_cp(*L1_idx, L1_tag_fixed, L1_associativity, L3_cache[0], debug);
    L3_expected_coherence = INVALID;

    EXPECT_EQ(status, OK);
    EXPECT_EQ(L1_expected_coherence, L1_coherence);
    EXPECT_EQ(L3_expected_coherence, L3_coherence);
    EXPECT_EQ(invalidation, false); // Avoids invalidation transaction

    for (int l = 0; l < 2; l++) {
        loadstore = 1-l; // Uses this logic to write first
        if (loadstore) {
            DEBUG(\nChecking write access form CPU1 invalidating CPU2);
            status = MESI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug); // Writes in L1

            if (debug){
                printf("\nL1 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
                }
                printf("\nL2 cache: \n");
                for(int i = 0; i < L2_associativity; i++){
                    printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
                }
                printf("\nL3 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
                }
                printf("\n");
            }

            L1_expected_coherence = MODIFIED;
            int L1_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L1_cache[0], debug);
            int L3_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L3_cache[0], debug);
            L3_expected_coherence = INVALID;

            EXPECT_EQ(status, OK);
            EXPECT_EQ(L1_expected_coherence, L1_coherence);
            EXPECT_EQ(L3_expected_coherence, L3_coherence);
            EXPECT_EQ(invalidation, true); // Expects invalidation from writing
        } else {
            DEBUG(\nChecking read access form CPU1);
            status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug); // Reads for L1

            if (debug){
                printf("\nL1 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
                }
                printf("\nL2 cache: \n");
                for(int i = 0; i < L2_associativity; i++){
                    printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
                }
                printf("\nL3 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
                }
                printf("\n");
            }

            L1_expected_coherence = SHARED;
            int L1_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L1_cache[0], debug);
            L3_expected_coherence = INVALID;

            EXPECT_EQ(status, OK);
            EXPECT_EQ(L1_expected_coherence, L1_coherence);
            EXPECT_EQ(invalidation, false);
        }
    }

    // Returns to CPU1: MODIFIED state and CPU2: INVALID state
    status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, 1/*writes*/, &invalidation, L1_cache[0], L2_cache[0], L3_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug);

    DEBUG(\nChecking CPU2 operations);
    for (int l = 0; l < 2; l++) {
        loadstore = l; // reads first
        if (!loadstore) {
            DEBUG(\nChecking read access form CPU2);
            status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L3_cache[0], L2_cache[0], L1_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug); // Reads for L3

            if (debug){
                printf("\nL1 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
                }
                printf("\nL2 cache: \n");
                for(int i = 0; i < L2_associativity; i++){
                    printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
                }
                printf("\nL3 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
                }
                printf("\n");
            }

            L1_expected_coherence = SHARED;
            int L1_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L1_cache[0], debug);
            int L3_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L3_cache[0], debug);
            L3_expected_coherence = SHARED;

            EXPECT_EQ(status, OK);
            EXPECT_EQ(L1_expected_coherence, L1_coherence);
            EXPECT_EQ(L3_expected_coherence, L3_coherence);
            EXPECT_EQ(invalidation, false);
        } else {
            DEBUG(\nChecking write access form CPU2 invalidating CPU1);
            status = MSI_replacement( *L1_idx, *L2_idx, *L3_tag, *L2_tag, L1_associativity, L2_associativity, *L1_tag_size, *L2_tag_size, *L1_idx_size, *L2_idx_size, *L1_offset_size, *L2_offset_size, loadstore, &invalidation, L3_cache[0], L2_cache[0], L1_cache[0], &L1_result, &L2_result, &L2_result_tmp, debug); // Writes in L3

            if (debug){
                printf("\nL1 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L1_cache[*L1_idx][i].tag, L1_cache[*L1_idx][i].cp_value);
                }
                printf("\nL2 cache: \n");
                for(int i = 0; i < L2_associativity; i++){
                    printf("Entry: %d Tag: %d | ", i, L2_cache[*L2_idx][i].tag);
                }
                printf("\nL3 cache: \n");
                for(int i = 0; i < L1_associativity; i++){
                    printf("Entry: %d Tag: %d CP: %d | ", i, L3_cache[*L1_idx][i].tag, L3_cache[*L1_idx][i].cp_value);
                }
                printf("\n");
            }

            L1_expected_coherence = INVALID;
            int L1_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L1_cache[0], debug);
            int L3_coherence = check_cp(*L1_idx, *L3_tag, L1_associativity, L3_cache[0], debug);
            L3_expected_coherence = MODIFIED;

            EXPECT_EQ(status, OK);
            EXPECT_EQ(L1_expected_coherence, L1_coherence);
            EXPECT_EQ(L3_expected_coherence, L3_coherence);
            EXPECT_EQ(invalidation, true); // Expects invalidation from writing
        }
    }

    // Frees memory allocation
    free(L1_tag_size);
    free(L1_idx_size);
    free(L1_offset_size);
    free(L2_tag_size);
    free(L2_idx_size);
    free(L2_offset_size);
    free(L1_idx);
    free(L2_idx);
    free(L2_idx_ignore);
    free(L1_tag);
    free(L2_tag);
    free(L3_tag);
}
/*
 * Gtest main function: Generates random seed, if not provided,
 * parses DEBUG flag, and execute the test suite
 */
int main(int argc, char **argv) {
    int argc_to_pass = 0;
    char **argv_to_pass = NULL;
    int seed = 0;

    /* Generate seed */
    seed = time(NULL) & 0xffff;

    /* Parse arguments looking if random seed was provided */
    argv_to_pass = (char **)calloc(argc + 1, sizeof(char *));

    for (int i = 0; i < argc; i++){
        std::string arg = std::string(argv[i]);

        if (!arg.compare(0, 20, "--gtest_random_seed=")){
            seed = atoi(arg.substr(20).c_str());
            continue;
        }
        argv_to_pass[argc_to_pass] = strdup(arg.c_str());
        argc_to_pass++;
    }

    /* Init Gtest */
    ::testing::GTEST_FLAG(random_seed) = seed;
    testing::InitGoogleTest(&argc, argv_to_pass);

    /* Print seed for debug */
    printf(YEL "Random seed %d \n",seed);
    srand(seed);

    /* Parse for debug env variable */
    get_env_var("TEST_DEBUG", &debug_on);

    /* Execute test */
    return RUN_ALL_TESTS();

    /* Free memory */
    free(argv_to_pass);

    return 0;
}
